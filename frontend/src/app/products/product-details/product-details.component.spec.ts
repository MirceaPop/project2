import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { of } from 'rxjs';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { RatingComponent } from 'src/app/shared/rating/rating.component';
import { ProductListWrapperComponent } from '../product-list-wrapper/product-list-wrapper.component';
import { AddProductComponent } from '../product-manager/add-product/add-product.component';
import { ProductService } from '../product.service';
import { ProductDetailsComponent } from './product-details.component';

describe('ProductFilterComponent tests', () => {
  let component: ProductDetailsComponent;
  let fixture: ComponentFixture<ProductDetailsComponent>;
  let productServiceMock: jasmine.SpyObj<ProductService>;
  let router: Router;

  productServiceMock = jasmine.createSpyObj<ProductService>('ProductService', [
    'getSpecificProduct',
  ]);

  productServiceMock.getSpecificProduct.and.returnValue(
    of({
      id: 1,
      name: 'aa',
      description: 'aa',
      price: 1,
      rating: 1,
      image:
        'https://ssl-product-images.www8-hp.com/digmedialib/prodimg/lowres/c07049591.png',
      tags: [],
    })
  );

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductDetailsComponent, RatingComponent],
      imports: [
        TranslateModule.forRoot(),
        NgxsModule.forRoot([ProductState]),
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          {
            path: 'products',
            component: ProductListWrapperComponent,
          },
          {
            path: 'products/manager/edit-product/1',
            component: AddProductComponent,
          },
        ]),
      ],
      providers: [{ provide: ProductService, useValue: productServiceMock }],
    });

    fixture = TestBed.createComponent(ProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.inject(Router);
  });

  it('should check that the component is defined', () => {
    expect(component).toBeTruthy();
  });

  it('should check that the product details are displayed correctly', fakeAsync(() => {
    // given
    const nameLiElement = fixture.nativeElement.querySelector(
      "li[li-name='nameListElement']"
    );
    const descriptionLiElement = fixture.nativeElement.querySelector(
      "li[li-name='descriptionListElement']"
    );
    const priceLiElement = fixture.nativeElement.querySelector(
      "li[li-name='priceListElement']"
    );
    const imageElement = fixture.nativeElement.querySelector(
      "img[img-name='imageElement']"
    );

    // then
    expect(imageElement).toBeTruthy();
    expect(nameLiElement.innerHTML).toContain('aa');
    expect(descriptionLiElement.innerHTML).toContain('aa');
    expect(priceLiElement.innerHTML).toContain(1);
  }));

  [
    { buttonName: 'navigateToProductsButton', expectedResult: '/products' },
    {
      buttonName: 'navigateToProductManagerButton',
      expectedResult: '/products/manager/edit-product/1',
    },
  ].forEach((element) => {
    it('should go to the correct urls', fakeAsync(() => {
      // given
      const buttonElement = fixture.nativeElement.querySelector(
        `button[button-name=${element.buttonName}]`
      );

      // when
      buttonElement.click();
      tick();

      // then
      expect(router.url).toBe(element.expectedResult);
    }));
  });

  it('should call sendProduct method on click', () => {
    // given
    spyOn(component, 'sendProduct');
    const buttonElement = fixture.nativeElement.querySelector(
      'button[button-name="addProductToCartButton"]'
    );

    // when
    buttonElement.click();

    // then
    expect(component.sendProduct).toHaveBeenCalled();
  });
});
