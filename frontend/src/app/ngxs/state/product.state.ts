import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable, tap } from 'rxjs';
import {
  ProductListModel,
  ProductModel,
  SimpleProductsModel,
  TagModel,
} from 'src/app/products/product.model';
import { ProductService } from 'src/app/products/product.service';
import {
  GetProductById,
  GetProductsByFilters,
  GetProductsByIds,
  GetTags,
  SendProduct,
  SendTag,
} from '../action/product.actions';

export interface ProductStateModel {
  specificProduct?: ProductModel;
  responseStatusCode: number;
  tags: TagModel[];
  productsList: ProductListModel;
  productsAfterIds: SimpleProductsModel;
}

@State<ProductStateModel>({
  name: 'productState',
  defaults: {
    productsAfterIds: { products: [] },
    tags: [],
    productsList: { products: [], sizeResult: 0, total: 0 },
    responseStatusCode: 0,
    specificProduct: undefined,
  },
})
@Injectable()
export class ProductState {
  constructor(private productService: ProductService) {}

  @Selector()
  static getProductAfterId(state: ProductStateModel) {
    return state.specificProduct;
  }

  @Selector()
  static sendProduct(state: ProductStateModel) {
    return state.responseStatusCode;
  }

  @Selector()
  static getProductsAfterIds(state: ProductStateModel) {
    return state.productsAfterIds;
  }

  @Selector()
  static getProductsAfterFilters(state: ProductStateModel) {
    return state.productsList;
  }

  @Selector()
  static getTags(state: ProductStateModel) {
    return state.tags;
  }

  @Action(GetProductById)
  getProductAfterId(
    ctx: StateContext<ProductStateModel>,
    action: GetProductById
  ): Observable<ProductModel> {
    return this.productService.getSpecificProduct(action.productId).pipe(
      tap((result) => {
        ctx.setState({
          ...ctx.getState(),
          specificProduct: result,
        });
      })
    );
  }

  @Action(GetProductsByFilters)
  getProductsAfterFilters(
    ctx: StateContext<ProductStateModel>,
    action: GetProductsByFilters
  ): Observable<ProductListModel> {
    return this.productService.getProducts(action.filters).pipe(
      tap((result) => {
        ctx.setState({
          ...ctx.getState(),
          productsList: result,
        });
      })
    );
  }

  @Action(GetTags)
  getTags(ctx: StateContext<ProductStateModel>): Observable<TagModel[]> {
    return this.productService.getTags().pipe(
      tap((result) => {
        ctx.setState({
          ...ctx.getState(),
          tags: result,
        });
      })
    );
  }

  @Action(SendProduct)
  sendProduct(
    ctx: StateContext<ProductStateModel>,
    action: SendProduct
  ): Observable<HttpResponse<ProductModel>> {
    return this.productService.sendProduct(action.product).pipe(
      tap((statusCode) => {
        ctx.setState({
          ...ctx.getState(),
          responseStatusCode: statusCode.status,
        });
      })
    );
  }

  @Action(SendTag)
  sendTag(ctx: StateContext<ProductStateModel>, action: SendTag) {
    this.productService.sendTag(action.tag).subscribe();
    ctx.dispatch(new GetTags());
    return ctx.setState(ctx.getState());
  }

  @Action(GetProductsByIds)
  getProductsAfterIds(
    ctx: StateContext<ProductStateModel>,
    action: GetProductsByIds
  ): Observable<SimpleProductsModel> {
    return this.productService.getProductsAfterIds(action.productsIds).pipe(
      tap((result) => {
        ctx.setState({
          ...ctx.getState(),
          productsAfterIds: result,
        });
      })
    );
  }
}
