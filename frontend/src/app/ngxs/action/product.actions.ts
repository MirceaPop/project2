import {
  ProductFiltersModel,
  ProductModel,
  TagModel,
} from 'src/app/products/product.model';

export class GetProductById {
  static readonly type = '[Product State] GetProductById';
  constructor(public productId: number) {}
}

export class GetProductsByIds {
  static readonly type = '[Product State] GetProductsByIds';
  constructor(public productsIds: number[]) {}
}

export class GetProductsByFilters {
  static readonly type = '[Product State] GetProductsByFilters';
  constructor(public filters: ProductFiltersModel) {}
}

export class GetTags {
  static readonly type = '[Product State] GetTags';
}

export class SendProduct {
  static readonly type = '[Product State] SendProduct';
  constructor(public product: ProductModel) {}
}

export class SendTag {
  static readonly type = '[Product State] SendTag';
  constructor(public tag: TagModel) {}
}

export class DeleteProduct {
  static readonly type = '[Product State] DeleteProduct';
  constructor(public productId: number) {}
}
