import { LocalStorageService } from './local-storage.service';

describe('LocalStorageService test', () => {
  const localStorageService = new LocalStorageService();
  let spyOnGetItem: jasmine.Spy;

  beforeEach(() => {
    spyOnGetItem = spyOn(localStorage, 'getItem');
  });

  it('should return number when getFromLocalStorage is called', () => {
    spyOnGetItem.and.returnValue('123');
    const result = localStorageService.getFromLocalStorage<number>('');
    expect(result).toBe(123);
  });

  it('should return boolean when getFromLocalStorage is called', () => {
    spyOnGetItem.and.returnValue('true');
    const result = localStorageService.getFromLocalStorage<boolean>('key');
    expect(result).toBe(true);
  });

  it('should return number when getFromLocalStorage is called', () => {
    spyOnGetItem.and.returnValue('abc');
    const result = localStorageService.getFromLocalStorage<string>('key');
    expect(result).toBe('abc');
  });

  it('should return object when getFromLocalStorage is called', () => {
    spyOnGetItem.and.returnValue(JSON.stringify({ key: 'value' }));
    const result = localStorageService.getFromLocalStorage<object>('key');
    expect(result).toEqual({ key: 'value' });
  });
});
