import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AddProductToCart } from 'src/app/ngxs/action/cart.actions';
import { ProductModel } from 'src/app/products/product.model';
import { CartModel } from 'src/app/shared/cart.model';
import { DEFAULT_PRODUCT_IMAGE } from 'src/app/shared/constants';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css'],
})
export class ProductCardComponent {
  src: string;
  fieldsData: ProductModel;
  currentItem: CartModel;

  @Input()
  set product(product: ProductModel) {
    this.src = product.image;
    this.fieldsData = product;
  }

  constructor(private router: Router, private store: Store) {}

  sendProduct() {
    this.store.dispatch(new AddProductToCart(this.fieldsData));
  }

  changeImage() {
    this.src = DEFAULT_PRODUCT_IMAGE;
  }

  onNavigateToProductClick() {
    this.router.navigate(['/products/details', this.fieldsData.id]);
  }
}
