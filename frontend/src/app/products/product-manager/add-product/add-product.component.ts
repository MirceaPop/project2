import { HttpStatusCode } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import {
  concatMap,
  filter,
  map,
  Observable,
  of,
  Subject,
  take,
  takeUntil,
  tap,
} from 'rxjs';
import {
  GetProductById,
  GetProductsByFilters,
  GetTags,
  SendProduct,
} from 'src/app/ngxs/action/product.actions';
import { ProductState } from 'src/app/ngxs/state/product.state';
import {
  ProductFormUseCase,
  ProductModel,
  TagModel,
} from '../../product.model';
import { ProductService } from '../../product.service';

interface ProductAndTagsResponse {
  product: ProductModel;
  tags: TagModel[];
}

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
})
export class AddProductComponent implements OnInit {
  readonly NAME_INPUT_ERROR_MESSAGE =
    'Field is required, name must be at least 4 characters long and at max 10.';
  readonly REQUIRED_ERROR_MESSAGE = 'Field is required.';
  readonly PRICE_ERROR_MESSAGE =
    'Field is required and maximum price should be 10000.';

  stars: number = 0;
  tagList: TagModel[] = [];
  isOnEdit: boolean;
  productForm: FormGroup;
  selectedTagsNo: number = 0;
  isSubmitError: boolean = false;
  doesProductExist: boolean = true;
  private productId: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store
  ) {}

  @Select(ProductState.getProductAfterId)
  receivedProduct$: Observable<ProductModel>;

  @Select(ProductState.getTags)
  receivedTags$: Observable<TagModel[]>;

  ngOnInit(): void {
    this.store.dispatch(new GetTags());
    this.isOnEdit =
      this.activatedRoute.snapshot.data['productFormUseCase'] ===
      ProductFormUseCase.EDIT;
    if (this.isOnEdit) {
      this.productId = Number(this.route.snapshot.paramMap.get('id'));
      this.store.dispatch(new GetProductById(this.productId));
      this.doesProductExist = false;
      this.receivedProduct$
        .pipe(
          filter((product) => !!product),
          take(1),
          tap((product) => this.initializeForm(product, this.isOnEdit)),
          concatMap((product) =>
            this.receivedTags$.pipe(
              filter((tags) => !!tags.length),
              take(1),
              map((tags) => {
                return { product, tags } as ProductAndTagsResponse;
              })
            )
          )
        )
        .subscribe(({ product, tags }: ProductAndTagsResponse) => {
          this.doesProductExist = true;
          this.checkAndSelectTags(product, tags);
        });
    } else {
      this.initializeForm();
      this.receivedTags$
        .pipe(
          filter((tags) => !!tags),
          take(1)
        )
        .subscribe((tags) => {
          this.tagList = tags.map((tag) => ({ ...tag, isSelected: false }));
        });
    }
  }

  rateProduct(rating: number) {
    this.stars = rating;
  }

  submitForm() {
    const product = this.productForm.value;
    product.rating = this.stars;
    product.tags = [];
    product.id = this.productId;
    this.tagList.forEach((tag) => {
      if (tag.isSelected === true) product.tags.push(tag);
    });
    this.store.dispatch(new SendProduct(product));
    this.resetForm();
    if (this.isOnEdit) {
      this.router.navigateByUrl('products/manager/new-product');
    }
  }

  deleteProduct() {
    this.productService
      .deleteProduct(this.productId)
      .pipe(take(1))
      .subscribe({
        next: () => this.router.navigateByUrl('products/manager/new-product'),
      });
  }

  selectTag(index: number) {
    if (this.tagList[index].isSelected) {
      this.selectedTagsNo -= 1;
    } else {
      this.selectedTagsNo += 1;
    }
    this.tagList[index].isSelected = !this.tagList[index].isSelected;
  }

  checkPrice(event: any) {
    if (
      (event.target.value.length === 0 && event.key === '0') ||
      event.key === '-' ||
      event.key === 'e'
    ) {
      event.preventDefault();
    }
  }

  // // Boolean for the guard to check if the user can leave the page or not
  canDeactivate(): Observable<boolean> | boolean {
    if (this.productForm?.dirty) {
      const result = window.confirm('There are unsaved changes! Are you sure?');

      return of(result);
    }
    return true;
  }

  private resetForm() {
    this.productForm.reset();
    this.stars = 0;
    this.tagList.forEach((tag) => (tag.isSelected = false));
  }

  private checkAndSelectTags(product: ProductModel, tags: TagModel[]) {
    let newTagList = [];
    for (let tag of tags) {
      for (let selectedTag of product.tags) {
        if (selectedTag.id === tag.id) {
          newTagList.push({ ...tag, isSelected: true });
          this.selectedTagsNo += 1;
          break;
        }
        newTagList.push({ ...tag, isSelected: false });
      }
      this.tagList = newTagList;
    }
  }

  private initializeForm(
    productToUpdate?: ProductModel,
    isOnEdit: boolean = false
  ) {
    this.productForm = new FormGroup({
      name: new FormControl(isOnEdit ? productToUpdate?.name : '', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(20),
      ]),
      description: new FormControl(
        isOnEdit ? productToUpdate?.description : '',
        [Validators.required]
      ),
      image: new FormControl(isOnEdit ? productToUpdate?.image : '', [
        Validators.required,
      ]),
      price: new FormControl(isOnEdit ? productToUpdate?.price : '', [
        Validators.required,
        Validators.max(10000),
      ]),
    });
    this.stars = productToUpdate?.rating || 0;
  }
}
