import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject, takeUntil } from 'rxjs';
import { AddProductToCart } from 'src/app/ngxs/action/cart.actions';
import { GetProductById } from 'src/app/ngxs/action/product.actions';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { DEFAULT_PRODUCT_IMAGE } from 'src/app/shared/constants';
import { ProductModel } from '../product.model';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit {
  product: ProductModel;
  private destroy: Subject<boolean> = new Subject<boolean>();

  constructor(
    private store: Store,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  @Select(ProductState.getProductAfterId)
  receivedProduct$: Observable<ProductModel>;
  receivedProduct: ProductModel;

  ngOnInit() {
    const productId = Number(this.activatedRoute.snapshot.params['id']);
    this.store.dispatch(new GetProductById(productId));
    this.receivedProduct$.pipe(takeUntil(this.destroy)).subscribe((product) => {
      this.product = product;
    });
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  changeImage() {
    if (this.product) {
      this.product = { ...this.product, image: DEFAULT_PRODUCT_IMAGE };
    }
  }

  sendProduct() {
    this.store.dispatch(new AddProductToCart(this.product));
  }

  onNavigateToEditProductClick() {
    this.router.navigate(['/products/manager/edit-product', this.product.id]);
  }

  onNavigateToProductsClick() {
    this.router.navigate(['/products']);
  }
}
