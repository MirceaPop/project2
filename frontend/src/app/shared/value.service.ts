import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ValueService {
  constructor() {}

  isGreaterThanFive(param: number) {
    return param > 5 ? true : false;
  }

  addValues(a: number, b: number) {
    return a + b;
  }
}
