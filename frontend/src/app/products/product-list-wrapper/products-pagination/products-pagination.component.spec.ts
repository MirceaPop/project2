import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { ProductsPaginationComponent } from './products-pagination.component';

describe('ProductsPaginationComponent tests', () => {
  let component: ProductsPaginationComponent;
  let fixture: ComponentFixture<ProductsPaginationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductsPaginationComponent],
      imports: [TranslateModule.forRoot()],
    });

    fixture = TestBed.createComponent(ProductsPaginationComponent);
    component = fixture.componentInstance;
    component.productsPerPageFilter = 3;
    component.totalNumberOfItems = 30;
    fixture.detectChanges();
  });

  it('should check that the component is defined', () => {
    expect(component).toBeTruthy();
  });

  it('should check that no buttons are displayed if there are no pages', () => {
    // given
    component.numberOfPages = 0;

    // when
    fixture.detectChanges();
    const previousPageButton = fixture.debugElement.queryAll(By.css('button'));

    // then
    expect(previousPageButton.length).toBe(0);
  });

  it('should check that both buttons are disabled if there is only 1 page', () => {
    // given
    component.productsPerPageFilter = 3;
    component.totalNumberOfItems = 3;

    // when
    fixture.detectChanges();
    const previousPageButton: HTMLButtonElement =
      fixture.nativeElement.querySelector(
        "button[button-name='previousPageButton']"
      );
    const nextPageButton: HTMLButtonElement =
      fixture.nativeElement.querySelector(
        "button[button-name='nextPageButton']"
      );

    // then
    expect(previousPageButton.disabled).toBeTruthy();
    expect(nextPageButton.disabled).toBeTruthy();
  });

  [
    { currentItemIndex: 0, expectedTimesToBeCalled: 0 },
    { currentItemIndex: 1, expectedTimesToBeCalled: 1 },
    { currentItemIndex: 2, expectedTimesToBeCalled: 1 },
  ].forEach((element) => {
    it('should check that the previous button is disabled at first page and works on other pages', () => {
      // given
      spyOn(component, 'onPrevChange');
      component.currentItemIndex = element.currentItemIndex;

      // when
      fixture.detectChanges();
      const previousPageButton: HTMLButtonElement =
        fixture.nativeElement.querySelector(
          "button[button-name='previousPageButton']"
        );
      previousPageButton.click();
      fixture.detectChanges();

      // then
      expect(component.onPrevChange).toHaveBeenCalledTimes(
        element.expectedTimesToBeCalled
      );
    });
  });

  [
    { currentItemIndex: 0, expectedTimesToBeCalled: 1 },
    { currentItemIndex: 1, expectedTimesToBeCalled: 1 },
    { currentItemIndex: 9, expectedTimesToBeCalled: 0 },
  ].forEach((element) => {
    it('should check that the next button is disabled at last page and works on other pages', () => {
      // given
      spyOn(component, 'onNextChange');
      component.currentItemIndex = element.currentItemIndex;

      // when
      fixture.detectChanges();
      const nextPageButton: HTMLButtonElement =
        fixture.nativeElement.querySelector(
          "button[button-name='nextPageButton']"
        );
      nextPageButton.click();
      fixture.detectChanges();

      // then
      expect(component.onNextChange).toHaveBeenCalledTimes(
        element.expectedTimesToBeCalled
      );
    });
  });

  it('should test that the ... appear on left and right when the user is on middle page', () => {
    // given
    component.modifyPagination(5);

    // when
    fixture.detectChanges();

    // then
    expect(
      fixture.debugElement.queryAll(By.css('li'))[1].nativeElement
    ).toMatch(`...`);
    expect(
      fixture.debugElement.queryAll(By.css('li'))[7].nativeElement
    ).toMatch(`...`);
  });

  [
    { pageToClick: 1, numberOfPagesDisplayed: 6 },
    { pageToClick: 2, numberOfPagesDisplayed: 7 },
    { pageToClick: 4, numberOfPagesDisplayed: 5 },
  ].forEach((element) => {
    it('should test when the user clicks on a specific page it modifies the current pageIndex and displays the corect number of pages', () => {
      // given
      const elementToClick = fixture.debugElement.queryAll(By.css('a'))[
        element.pageToClick
      ].nativeElement;

      // when
      elementToClick.dispatchEvent(new Event('click'));
      fixture.detectChanges();

      // then
      expect(
        fixture.debugElement.queryAll(By.css('li'))[element.pageToClick]
          .nativeElement
      ).toHaveClass('active');
      expect(fixture.debugElement.queryAll(By.css('li')).length).toBe(
        element.numberOfPagesDisplayed
      );
    });
  });
});
