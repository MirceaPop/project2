import { Injectable } from '@angular/core';
import { ValueService } from './value.service';

@Injectable({
  providedIn: 'root',
})
export class MasterService {
  constructor(private valueService: ValueService) {}

 isGreaterThan(param: number){
  return this.valueService.isGreaterThanFive(param)? `${param} is greater than 5` : `${param} is less than 5`
 }

  getValue(param: number) {
    return this.valueService.isGreaterThanFive(param);
  }

  getAddedValues(a: number, b: number) {
    return this.valueService.addValues(a, b);
  }
}
