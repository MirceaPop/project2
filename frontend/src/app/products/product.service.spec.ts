import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { hostUrl } from '../appConfig';
import {
  ProductFiltersModel,
  ProductListModel,
  ProductModel,
  SimpleProductsModel,
  Sort,
  TagModel,
} from './product.model';
import { ProductService } from './product.service';

describe('ProductService tests', () => {
  let productService: ProductService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductService],
    });
    productService = TestBed.inject(ProductService);
    // httpTestingController allows for mocking and flushing requests
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should retrieve all tags', (done: DoneFn) => {
    // given
    const dummyTags: TagModel[] = [
      { id: 1, name: 'laptop' },
      { id: 2, name: 'phone' },
    ];
    const $expectedTags = productService.getTags();

    // when + then
    $expectedTags.subscribe((tags) => {
      expect(tags.length).toBe(2);
      expect(tags).toEqual(dummyTags);
      done();
    });

    const request = httpMock.expectOne(`${hostUrl}/tags`);
    request.flush(dummyTags);

    expect(request.request.method).toBe('GET');
  });

  it('should retrive product after id', (done: DoneFn) => {
    // given
    const dummyProduct: ProductModel = {
      id: 2,
      name: 'product',
      description: 'description',
      price: 2,
      rating: 2,
      image: 'url',
      tags: [],
    };
    const id = 1;
    const expectedProduct = productService.getSpecificProduct(id);

    // when + then
    expectedProduct.subscribe((product) => {
      expect(product).toEqual(dummyProduct);
      done();
    });

    const request = httpMock.expectOne(`${hostUrl}/products/${id}`);
    expect(request.request.method).toBe('GET');

    request.flush(dummyProduct);
  });

  it('should retrieve all products with regards to the filters', (done: DoneFn) => {
    // given
    const dummyFilters: ProductFiltersModel = {
      tagData: '',
      priceOrder: Sort.ASC,
      ratingOrder: Sort.ASC,
      itemsNumber: 5,
      pageNumber: 0,
    };
    const dummyProducts: ProductListModel = {
      total: 2,
      sizeResult: 2,
      products: [],
    };
    const expectedProducts = productService.getProducts(dummyFilters);

    // when + then
    expectedProducts.subscribe((products) => {
      expect(products).toEqual(dummyProducts);
      done();
    });

    const request = httpMock.expectOne(
      `${hostUrl}/products?pageNo=0&pageSize=5&priceSort=ASC&ratingSort=ASC&tag=`
    );
    expect(request.request.method).toBe('GET');

    request.flush(dummyProducts);
  });

  it('should get products after a provided list of ids', (done: DoneFn) => {
    // given
    const dummyIds: number[] = [1];
    const dummyProducts: SimpleProductsModel = {
      products: [
        {
          id: 1,
          name: 'product',
          description: 'description',
          price: 2,
          rating: 2,
          image: 'url',
          tags: [],
        },
      ],
    };
    const products = productService.getProductsAfterIds(dummyIds);

    // when + then
    products.subscribe((products) => {
      expect(products).toEqual(dummyProducts);
      done();
    });

    const request = httpMock.expectOne(`${hostUrl}/cart/products?productId=1`);
    expect(request.request.method).toBe('GET');

    request.flush(dummyProducts);
  });

  it('should send a tag', (done: DoneFn) => {
    // given
    const dummyTag: TagModel = { id: 1, name: 'phone' };
    const tag = productService.sendTag(dummyTag);

    // when + then
    tag.subscribe((response) => {
      expect(response).toEqual(dummyTag);
      done();
    });

    const request = httpMock.expectOne(`${hostUrl}/tags`);
    expect(request.request.method).toBe('POST');

    request.flush(dummyTag);
  });

  it('should send a product', (done: DoneFn) => {
    // given
    const dummyProduct: ProductModel = {
      id: 1,
      name: 'product',
      description: 'description',
      price: 2,
      rating: 2,
      image: 'url',
      tags: [],
    };
    const product = productService.sendProduct(dummyProduct);

    // when + then
    product.subscribe((response) => {
      expect(response.body).toEqual(dummyProduct);
      done();
    });

    const request = httpMock.expectOne(`${hostUrl}/products`);
    expect(request.request.method).toBe('POST');

    request.flush(dummyProduct);
  });

  it('should delete product', (done: DoneFn) => {
    // given
    const dummyId = 1;
    const responseStatus = 200;
    const productToDelete = productService.deleteProduct(dummyId);

    // when + then
    productToDelete.subscribe((response) => {
      expect(response).toBe(responseStatus);
      done();
    });

    const request = httpMock.expectOne(`${hostUrl}/products/${dummyId}`);
    expect(request.request.method).toBe('DELETE');

    request.flush(responseStatus);
  });
});
