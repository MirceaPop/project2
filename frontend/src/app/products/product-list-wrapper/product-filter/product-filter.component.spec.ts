import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { ErrorWrapperComponent } from 'src/app/shared/error-wrapper/error-wrapper.component';
import { ProductFilterComponent } from './product-filter.component';

describe('ProductFilterComponent tests', () => {
  let component: ProductFilterComponent;
  let fixture: ComponentFixture<ProductFilterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductFilterComponent, ErrorWrapperComponent],
      imports: [
        TranslateModule.forRoot(),
        NgxsModule.forRoot([]),
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
    });

    fixture = TestBed.createComponent(ProductFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should check that the component is defined', () => {
    expect(component).toBeTruthy();
  });

  [
    { selectedElement: 0, labelValue: 'FILTER.DESCENDING' },
    { selectedElement: 1, labelValue: 'FILTER.DESCENDING' },
    { selectedElement: 2, labelValue: '10' },
  ].forEach((element) => {
    it('should change the value on priceOrder selection change and call onFiltersChanged method', fakeAsync(() => {
      // given
      spyOn<any>(component, 'onFiltersChanged');
      const select: HTMLSelectElement = fixture.debugElement.queryAll(
        By.css('select')
      )[element.selectedElement].nativeElement;
      select.value = select.options[1].value;

      // when
      select.dispatchEvent(new Event('change'));
      const labelText = select.options[select.selectedIndex].label;
      tick(500);

      // then
      expect(labelText).toBe(element.labelValue);
      expect(component['onFiltersChanged']).toHaveBeenCalled();
    }));
  });

  it('should change the filters input value when we change the filters value', fakeAsync(() => {
    // given
    const inputElement: HTMLInputElement = fixture.debugElement.query(
      By.css('input')
    ).nativeElement;
    inputElement.value = 'tag';

    // when
    inputElement.dispatchEvent(new Event('input'));
    tick(500);

    // then
    expect(inputElement.value).toBe('tag');
    expect(component.filtersForm.controls['tagData'].value).toEqual('tag');
  }));
});
