import { HttpErrorResponse } from '@angular/common/http';

export class EmitError {
  static readonly type = '[Error State] EmitError';
  constructor(public errorResponse: HttpErrorResponse) {}
}

export class EmitSpinnerStatus {
  static readonly type = '[Error State] EmitSpinnerStatus';
  constructor(public isSpinnerLoading: boolean) {}
}
