import { CustomCurrencyPipe } from './custom-currency.pipe';

describe('CustomCurrencyPipe tests', () => {
  const customPipe: CustomCurrencyPipe = new CustomCurrencyPipe();

  it('should create an instance', () => {
    expect(customPipe).toBeTruthy();
  });

  [
    {
      initialValue: 12345.99,
      transformedValue: '12.345,99 €',
    },
    {
      initialValue: 123456,
      transformedValue: '123.456,00 €',
    },
    {
      initialValue: 1234567,
      transformedValue: '1.234.567,00 €',
    },
    {
      initialValue: 12345678.88,
      transformedValue: '12.345.678,88 €',
    },
    {
      initialValue: 123456789,
      transformedValue: '123.456.789,00 €',
    },
  ].forEach((number) => {
    it(`should tranform ${number.initialValue} to ${number.transformedValue}`, () => {
      // when
      const transformedValueFromCustomPipe = customPipe.transform(
        number.initialValue
      );

      // then
      expect(number.transformedValue).toBe(transformedValueFromCustomPipe);
    });
  });
});
