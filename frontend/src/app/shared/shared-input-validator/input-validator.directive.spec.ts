import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { By } from '@angular/platform-browser';
import { InputValidatorDirective } from './input-validator.directive';

@Component({
  template: `
    <form [formGroup]="dummyForm">
      <input type="text" formControlName="name" appInputValidator />
    </form>
  `,
})
class TestValidInputComponent {
  dummyForm: FormGroup;

  ngOnInit() {
    this.dummyForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(20),
      ]),
    });
  }
}

describe('custom directive test', () => {
  let component: TestValidInputComponent;
  let fixture: ComponentFixture<TestValidInputComponent>;
  let inputElement: HTMLInputElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [TestValidInputComponent, InputValidatorDirective],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestValidInputComponent);
    component = fixture.componentInstance;
    inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    fixture.detectChanges();
  });

  it('should set grey border-color when input is created', () => {
    expect(inputElement.style['borderColor']).toBe('grey');
  });

  [{ valueToTest: 'mircea' }, { valueToTest: '1234' }].forEach((value) => {
    it(`should set the input border-color to green when input value is ${value.valueToTest}`, () => {
      // given
      inputElement.value = value.valueToTest;

      // when
      inputElement.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      // then
      expect(inputElement.style['borderColor']).toBe('green');
    });
  });

  [{ valueToTest: 'mi' }, { valueToTest: ',' }].forEach((value) => {
    it(`should set the input border-color to red when input value is ${value.valueToTest}`, () => {
      // given
      inputElement.value = value.valueToTest;

      // when
      inputElement.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      // then
      expect(inputElement.style['borderColor']).toBe('red');
    });
  });
});
