package com.example.internship.model.response;

import com.example.internship.model.dto.ProductDTO;

import java.util.List;

public class ProductListResponse {

    private Long total;
    private Integer sizeResult;
    private List<ProductDTO> products;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getSizeResult() {
        return sizeResult;
    }

    public void setSizeResult(Integer sizeResult) {
        this.sizeResult = sizeResult;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }
}
