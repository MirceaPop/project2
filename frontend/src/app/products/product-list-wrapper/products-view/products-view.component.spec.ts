import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ProductCardComponent } from '../product-card/product-card.component';
import { ProductsViewComponent } from './products-view.component';
import { TranslateModule } from '@ngx-translate/core';
import { RatingComponent } from 'src/app/shared/rating/rating.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxsModule } from '@ngxs/store';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { environment } from 'src/environments/environment';
import { HttpClientTestingModule } from '@angular/common/http/testing';

const PRODUCTS = [
  {
    id: 1,
    name: 'product de calitate',
    description: 'bestProduct',
    price: 1,
    rating: 1,
    image: 'url',
    tags: [],
  },
  {
    id: 1,
    name: 'product de calitate',
    description: 'bestProduct',
    price: 1,
    rating: 1,
    image: 'url',
    tags: [],
  },
];

describe('products-view tests', () => {
  let component: ProductsViewComponent;
  let fixture: ComponentFixture<ProductsViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductsViewComponent,
        ProductCardComponent,
        RatingComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule,
        NgxsModule.forRoot([ProductState]),
      ],
    });

    fixture = TestBed.createComponent(ProductsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should check that the component is defined', () => {
    expect(component).toBeTruthy();
  });

  it('should check that the correct number of products are rendered', () => {
    // given
    component.products = PRODUCTS;

    // when
    fixture.detectChanges();
    const cardElement = fixture.debugElement.queryAll(
      By.css('app-product-card')
    );

    // then
    expect(cardElement.length).toBe(2);
  });
});
