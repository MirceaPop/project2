import { LitElement, html } from 'lit';
import { customElement } from 'lit/decorators.js';

import './MyHeader.js';
import './MyFooter.js';
import './MyArticle.js';

@customElement('my-page')
export class MyPage extends LitElement {
  render() {
    return html`
      <my-header></my-header>
      <my-article></my-article>
      <my-footer></my-footer>
    `;
  }
}
