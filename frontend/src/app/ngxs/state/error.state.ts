import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  GuardsCheckEnd,
  NavigationEnd,
  NavigationStart,
  Router,
} from '@angular/router';
import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { EmitError, EmitSpinnerStatus } from '../action/error.actions';

interface ErrorStateModel {
  errors: string[];
  isSpinnerLoading: boolean;
}

@State<ErrorStateModel>({
  name: 'errorState',
  defaults: {
    errors: [],
    isSpinnerLoading: true,
  },
})
@Injectable()
export class ErrorState implements NgxsOnInit {
  constructor(private router: Router) {}

  @Selector()
  static getErrors(state: ErrorStateModel) {
    return state.errors;
  }

  @Selector()
  static getSpinnerStatus(state: ErrorStateModel) {
    return state.isSpinnerLoading;
  }

  ngxsOnInit(ctx: StateContext<ErrorStateModel>) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        ctx.setState({ ...ctx.getState(), errors: [] });
        ctx.setState({ ...ctx.getState(), isSpinnerLoading: true });
      }
      if (event instanceof GuardsCheckEnd) {
        ctx.setState({ ...ctx.getState(), isSpinnerLoading: false });
      }
      if (event instanceof NavigationEnd) {
        ctx.setState({ ...ctx.getState(), isSpinnerLoading: false });
      }
    });
  }

  @Action(EmitError)
  emitError(ctx: StateContext<ErrorStateModel>, action: EmitError) {
    const errorMessage = this.setError(action.errorResponse);
    ctx.setState({
      ...ctx.getState(),
      errors: errorMessage,
    });
  }

  @Action(EmitSpinnerStatus)
  emitSpinnerStatus(
    ctx: StateContext<ErrorStateModel>,
    action: EmitSpinnerStatus
  ) {
    ctx.setState({
      ...ctx.getState(),
      isSpinnerLoading: action.isSpinnerLoading,
    });
  }

  setError(error: HttpErrorResponse): string[] {
    let errorMessage = 'Unknown error';
    if (error instanceof HttpErrorResponse) {
      if (error.status === 0) {
        errorMessage = 'Server is down';
      } else if (error.status === 400) {
        errorMessage = 'Bad Request';
      } else if (error.status === 404) {
        errorMessage = error.error.message;
      }
    }
    return [errorMessage];
  }
}
