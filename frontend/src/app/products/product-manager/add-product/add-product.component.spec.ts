import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { of } from 'rxjs';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { ErrorMessageComponent } from 'src/app/shared/error-message/error-message.component';
import { RatingComponent } from 'src/app/shared/rating/rating.component';
import { ProductService } from '../../product.service';
import { AddProductComponent } from './add-product.component';

describe('ProductsPaginationComponent tests', () => {
  let component: AddProductComponent;
  let fixture: ComponentFixture<AddProductComponent>;
  let productServiceMock: jasmine.SpyObj<ProductService>;

  productServiceMock = jasmine.createSpyObj<ProductService>('ProductService', [
    'getTags',
  ]);

  productServiceMock.getTags.and.returnValue(of([{ id: 1, name: 'name' }]));

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AddProductComponent,
        ErrorMessageComponent,
        RatingComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule,
        NgxsModule.forRoot([ProductState]),
        ReactiveFormsModule,
      ],
      providers: [{ provide: ProductService, useValue: productServiceMock }],
    });

    fixture = TestBed.createComponent(AddProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should check that the component is defined', () => {
    expect(component).toBeTruthy();
  });

  it('should check that on add-product case the submit form works', () => {
    // given
    const nameInputElement = fixture.nativeElement.querySelector(
      "input[input-name='nameElement']"
    );
    const descriptionInputElement = fixture.nativeElement.querySelector(
      "textarea[textarea-name='descriptionElement']"
    );
    const imageInputElement = fixture.nativeElement.querySelector(
      "input[input-name='imageElement']"
    );
    const priceInputElement = fixture.nativeElement.querySelector(
      "input[input-name='priceElement']"
    );

    const tagElement = fixture.nativeElement.querySelector(
      "span[span-name='tagElement']"
    );

    // when
    nameInputElement.value = 'aaaa';
    nameInputElement.dispatchEvent(new Event('input'));
    descriptionInputElement.value = 'aaaa';
    descriptionInputElement.dispatchEvent(new Event('input'));
    imageInputElement.value = 'aaaa';
    imageInputElement.dispatchEvent(new Event('input'));
    priceInputElement.value = 22;
    priceInputElement.dispatchEvent(new Event('input'));
    tagElement.click();
    fixture.detectChanges();

    // then
    expect(component.productForm.get('name')?.value).toBe('aaaa');
    expect(component.productForm.get('description')?.value).toBe('aaaa');
    expect(component.productForm.get('image')?.value).toBe('aaaa');
    expect(component.productForm.get('price')?.value).toBe(22);
    expect(component.productForm.status).toBe('VALID');
  });
});
