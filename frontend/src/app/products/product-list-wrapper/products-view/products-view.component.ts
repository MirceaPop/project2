import { Component, Input } from '@angular/core';
import { ProductModel } from 'src/app/products/product.model';

@Component({
  selector: 'app-products-view',
  templateUrl: './products-view.component.html',
  styleUrls: ['./products-view.component.css'],
})
export class ProductsViewComponent {
  @Input()
  products: ProductModel[];
}
