import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { of } from 'rxjs';
import {
  NumberOfItems,
  ProductListModel,
  ProductModel,
  SimpleProductsModel,
  Sort,
  TagModel,
} from 'src/app/products/product.model';
import { ProductService } from 'src/app/products/product.service';
import { environment } from 'src/environments/environment';
import {
  GetProductById,
  GetProductsByFilters,
  GetProductsByIds,
  GetTags,
  SendProduct,
  SendTag,
} from '../action/product.actions';
import { ProductState } from './product.state';

const DEFAULT_FILTERS = {
  tagData: '',
  priceOrder: Sort.ASC,
  ratingOrder: Sort.ASC,
  itemsNumber: NumberOfItems.FIVE,
  pageNumber: 0,
};

const SIMPLE_PRODUCTS: SimpleProductsModel = {
  products: [
    {
      id: 1,
      name: '',
      description: '',
      price: 1,
      rating: 1,
      image: '',
      tags: [],
    },
  ],
};

const PRODUCTS_RESPONSE: ProductListModel = {
  total: 1,
  sizeResult: 1,
  products: [
    {
      id: 1,
      name: '',
      description: '',
      price: 1,
      rating: 1,
      image: '',
      tags: [],
    },
  ],
};

const SINGLE_PRODUCT: ProductModel = {
  id: 1,
  name: 'abc',
  description: 'def',
  price: 1,
  rating: 2,
  image: '',
  tags: [],
};

const TAGS: TagModel[] = [
  { id: 1, name: 'tag1' },
  { id: 2, name: 'tag2' },
];

describe('Product State', () => {
  let store: Store;
  let productServiceMock: jasmine.SpyObj<ProductService>;

  beforeEach(() => {
    productServiceMock = jasmine.createSpyObj<ProductService>(
      'ProductService',
      [
        'getProducts',
        'getProductsAfterIds',
        'getTags',
        'getSpecificProduct',
        'sendProduct',
        'sendTag',
      ]
    );

    productServiceMock.getProducts.and.returnValue(of(PRODUCTS_RESPONSE));
    productServiceMock.getTags.and.returnValue(of(TAGS));
    productServiceMock.sendTag.and.returnValue(of({ id: 1, name: 'tag1' }));
    productServiceMock.sendProduct.and.returnValue(
      of(new HttpResponse({ status: 200 }) as HttpResponse<ProductModel>)
    );
    productServiceMock.getProductsAfterIds.and.returnValue(of(SIMPLE_PRODUCTS));
    productServiceMock.getSpecificProduct.and.returnValue(of(SINGLE_PRODUCT));

    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([ProductState]), HttpClientTestingModule],
      providers: [{ provide: ProductService, useValue: productServiceMock }],
    });

    store = TestBed.inject(Store);
    store.reset({
      productState: {
        products: [],
        productsAfterIds: { products: [] },
        totalNumberOfProducts: 0,
        sizeResult: 0,
        tags: [],
        responseStatusCode: 0,
        specificProduct: {
          id: 0,
          name: '',
          description: '',
          price: 0,
          rating: 0,
          image: '',
          tags: [],
        },
      },
    });
  });

  it('should test that the GetProductAfterId action works and changes the state', () => {
    // when
    store.dispatch(new GetProductById(1));

    const productsState = store.selectSnapshot(
      (state) => state.productState.specificProduct
    );

    // then
    expect(productsState).toEqual({
      id: 1,
      name: 'abc',
      description: 'def',
      price: 1,
      rating: 2,
      image: '',
      tags: [],
    });
  });

  it('should test that the GetProductsAfterFilters action works and changes the state', () => {
    // when
    store.dispatch(new GetProductsByFilters(DEFAULT_FILTERS));

    const productsState = store.selectSnapshot(
      (state) => state.productState.productsList.products
    );
    const sizeState = store.selectSnapshot(
      (state) => state.productState.productsList.sizeResult
    );

    // then
    expect(productsState).toEqual([
      {
        id: 1,
        name: '',
        description: '',
        price: 1,
        rating: 1,
        image: '',
        tags: [],
      },
    ]);
    expect(sizeState).toBe(1);
  });

  it('should test that the GetTags action works and changes the state', () => {
    // when
    store.dispatch(new GetTags());

    const tagsState = store.selectSnapshot((state) => state.productState.tags);

    // then
    expect(tagsState).toEqual([
      { id: 1, name: 'tag1' },
      { id: 2, name: 'tag2' },
    ]);
  });

  it('should test that the SendTag action works and changes the state', () => {
    // when
    store.dispatch(new SendTag({ id: 1, name: 'tag1' }));

    const tagsState = store.selectSnapshot((state) => state.productState.tags);

    // then
    expect(tagsState).toEqual([
      { id: 1, name: 'tag1' },
      { id: 2, name: 'tag2' },
    ]);
  });

  it('should test that the GetProductsAfterIds action works and changes the state', () => {
    // when
    store.dispatch(new GetProductsByIds([1, 2, 3]));

    const tagsState = store.selectSnapshot(
      (state) => state.productState.productsAfterIds
    );

    // then
    expect(tagsState).toEqual({
      products: [
        {
          id: 1,
          name: '',
          description: '',
          price: 1,
          rating: 1,
          image: '',
          tags: [],
        },
      ],
    });
  });

  it('should test that the SendProduct action works and changes the state', () => {
    // when
    store.dispatch(new SendProduct(SINGLE_PRODUCT));

    const responseState = store.selectSnapshot(
      (state) => state.productState.responseStatusCode
    );

    // then
    expect(responseState).toBe(200);
  });
});
