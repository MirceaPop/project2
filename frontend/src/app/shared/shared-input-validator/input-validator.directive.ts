import { Directive, HostBinding, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

enum BorderColor {
  DEFAULT = 'grey',
  VALID = 'green',
  INVALID = 'red',
}

@Directive({
  selector: '[appInputValidator]',
})
export class InputValidatorDirective {
  constructor(private ngControl: NgControl) {}

  @HostBinding('style.border-color')
  get borderColor() {
    const hasErrors = this.ngControl.control?.status === 'INVALID';
    const isTouchedOrDirty =
      this.ngControl.control?.touched || this.ngControl.control?.dirty;

    if (!isTouchedOrDirty) {
      return BorderColor.DEFAULT;
    } else {
      return hasErrors ? BorderColor.INVALID : BorderColor.VALID;
    }
  }
}
