import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { catchError, Observable, tap, throwError } from 'rxjs';
import {
  EmitError,
  EmitSpinnerStatus,
} from 'src/app/ngxs/action/error.actions';

@Injectable()
export class ErrorsInterceptor implements HttpInterceptor {
  private readonly linksToSkip = ['.*/i18n/en.json', '.*/cart/email-check'];

  constructor(private store: Store) {}
  // intercept transforms a request into an observable that eventualy returns the http response
  intercept(
    request: HttpRequest<any>,
    // next - urmatorul interceptor sau backendul daca nu ramane nici un interceptor
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (this.linksToSkip.find((value) => request.url.match(value))) {
      return next.handle(request);
    } else {
      this.store.dispatch(new EmitSpinnerStatus(true));
      // handle transforms the request into an observable of http events
      return next.handle(request).pipe(
        tap({
          // Succeeds when there is a response; ignore other events
          next: (event) => {
            if (event instanceof HttpResponse) {
              this.store.dispatch(new EmitSpinnerStatus(false));
            }
          },
        }),
        catchError((error: HttpErrorResponse) => {
          this.store.dispatch(new EmitError(error));
          this.store.dispatch(new EmitSpinnerStatus(false));
          return throwError(() => error);
        })
      );
    }
  }
}
