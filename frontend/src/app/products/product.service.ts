import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { hostUrl } from '../appConfig';
import {
  ProductFiltersModel,
  ProductListModel,
  ProductModel,
  SimpleProductsModel,
  TagModel,
} from './product.model';

@Injectable({ providedIn: 'root' })
export class ProductService {
  constructor(private httpClient: HttpClient) {}

  getProducts(filters: ProductFiltersModel) {
    const httpHeaders = new HttpHeaders({ responseType: 'application/json' });
    const httpParams = new HttpParams()
      .set('pageNo', filters.pageNumber)
      .set('pageSize', filters.itemsNumber)
      .set('priceSort', filters.priceOrder)
      .set('ratingSort', filters.ratingOrder)
      .set('tag', filters.tagData);

    return this.httpClient.get<ProductListModel>(`${hostUrl}/products`, {
      headers: httpHeaders,
      params: httpParams,
    });
  }

  getSpecificProduct(id: number) {
    const httpHeaders = new HttpHeaders({ responseType: 'application/json' });

    return this.httpClient.get<ProductModel>(`${hostUrl}/products/${id}`, {
      headers: httpHeaders,
    });
  }

  getProductsAfterIds(id: number[]) {
    const httpHeaders = new HttpHeaders({ responseType: 'application/json' });
    let httpParams = new HttpParams();
    httpParams = httpParams.append('productId', id.join(','));

    return this.httpClient.get<SimpleProductsModel>(
      `${hostUrl}/cart/products`,
      {
        headers: httpHeaders,
        params: httpParams,
      }
    );
  }

  getTags() {
    const httpHeaders = new HttpHeaders({ responseType: 'application/json' });

    return this.httpClient.get<TagModel[]>(`${hostUrl}/tags`, {
      headers: httpHeaders,
    });
  }

  sendTag(tagData: TagModel) {
    return this.httpClient.post<TagModel>(`${hostUrl}/tags`, tagData);
  }

  sendProduct(productData: ProductModel) {
    const httpResponse = new HttpResponse();
    const httpHeaders = new HttpHeaders({ responseType: 'application/json' });
    return this.httpClient.post<ProductModel>(
      `${hostUrl}/products`,
      productData,
      {
        headers: httpHeaders,
        observe: 'response',
      }
    );
  }

  deleteProduct(id: number) {
    return this.httpClient.delete(`${hostUrl}/products/${id}`);
  }
}
