import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { TagModel } from 'src/app/products/product.model';
import { ProductService } from 'src/app/products/product.service';
import { EmailService } from '../email-service/email.service';
import {
  tagInputValidatorFactory,
  emailValidatorFactory,
} from './custom-validators';

@Component({
  template: `
    <form [formGroup]="dummyForm">
      <input type="text" formControlName="name" />
      <input type="text" formControlName="email" />
    </form>
  `,
})
class TestValidInputComponent {
  dummyForm: FormGroup;
  tags: TagModel[] = [
    { id: 1, name: 'laptop' },
    { id: 1, name: 'phone' },
    { id: 1, name: 'pen' },
  ];

  constructor(private emailService: EmailService) {}

  ngOnInit() {
    this.dummyForm = new FormGroup({
      name: new FormControl('', [tagInputValidatorFactory(this.tags)]),
      email: new FormControl(
        '',
        [],
        [emailValidatorFactory(this.emailService)]
      ),
    });
  }
}

describe('custom-validators tests', () => {
  let component: TestValidInputComponent;
  let fixture: ComponentFixture<TestValidInputComponent>;
  let inputElement: HTMLInputElement;
  let fakeService: jasmine.SpyObj<EmailService>;

  beforeEach(() => {
    fakeService = jasmine.createSpyObj<EmailService>('EmailService', [
      'checkEmailExistance',
    ]);
    fakeService.checkEmailExistance.and.returnValue(
      of(new HttpResponse({ status: 200 }) as HttpResponse<void>)
    );

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [TestValidInputComponent],
      providers: [{ provide: EmailService, useValue: fakeService }],
    });

    fixture = TestBed.createComponent(TestValidInputComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  [
    { tag: 'laptop', expectedResult: true },
    { tag: 'phone', expectedResult: true },
    { tag: 'bike', expectedResult: false },
    { tag: 'car', expectedResult: false },
  ].forEach((element) => {
    it('should check that the form is invalid if input is the same as in tagsArray', () => {
      // given
      inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
      inputElement.value = element.tag;

      // when
      inputElement.dispatchEvent(new Event('input'));

      // then
      expect(component.dummyForm.get('name')?.invalid).toBe(
        element.expectedResult
      );
    });
  });

  it('should check if email is valid', fakeAsync(() => {
    // given
    inputElement = fixture.debugElement.queryAll(By.css('input'))[1]
      .nativeElement;
    inputElement.value = 'mircea@gmail.com';

    // when
    inputElement.dispatchEvent(new Event('input'));
    tick(500);

    // then
    expect(component.dummyForm.get('email')?.invalid).toBeFalsy();
  }));
});
