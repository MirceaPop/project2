package com.example.internship.model;

public enum SortDirection {
    ASC, DESC;
}
