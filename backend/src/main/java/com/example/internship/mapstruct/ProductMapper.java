package com.example.internship.mapstruct;

import com.example.internship.model.Product;
import com.example.internship.model.dto.ProductDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ProductMapper {
    ProductDTO productToProductDTOMapper(Product product);

    Product productDTOToProductMapper(ProductDTO product);

    List<Product> productDTOListToProductListMapper(List<ProductDTO> product);

    List<ProductDTO> productListToProductDTOListMapper(List<Product> product);
}
