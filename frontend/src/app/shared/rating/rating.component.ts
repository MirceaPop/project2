import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css'],
})
export class RatingComponent {
  ratings: number[] = [1, 2, 3, 4, 5];
  isMouseoverDisabled: boolean = false;

  @Input()
  productRating: number;

  @Output()
  newItemEvent = new EventEmitter<number>();

  onClick(i: number) {
    this.isMouseoverDisabled = true;
    this.newItemEvent.emit(i);
  }

  onMouseOver(i: number) {
    if (!this.isMouseoverDisabled) {
      this.newItemEvent.emit(i);
    }
  }
}
