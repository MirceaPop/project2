package com.example.internship.model.response;

import com.example.internship.model.Image;

public class ImageWithFrontendIndex {
    private Long index;
    private Image image;

    public ImageWithFrontendIndex() {
    }

    public ImageWithFrontendIndex(Long index, Image image) {
        this.index = index;
        this.image = image;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
