import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule, Store } from '@ngxs/store';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { RatingComponent } from 'src/app/shared/rating/rating.component';
import { environment } from 'src/environments/environment';
import { ProductModel } from '../../product.model';
import { ProductCardComponent } from './product-card.component';

const expectedProduct: ProductModel = {
  id: 1,
  name: 'product de calitate',
  description: 'bestProduct',
  price: 1,
  rating: 1,
  image: 'url',
  tags: [],
};

describe('ProductCardComponent tests', () => {
  let router: Router;
  let component: ProductCardComponent;
  let fixture: ComponentFixture<ProductCardComponent>;
  let store: Store;

  beforeEach(() => {
    const routerMock = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      declarations: [ProductCardComponent, RatingComponent],
      imports: [
        TranslateModule.forRoot(),
        NgxsModule.forRoot([ProductState]),
        HttpClientTestingModule,
      ],
      providers: [{ provide: Router, useValue: routerMock }],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    component.product = expectedProduct;
    store = TestBed.inject(Store);
    fixture.detectChanges();
  });

  it('should check that name, price and rating are showed', () => {
    // given
    const cardElement = fixture.debugElement.query(
      By.css('.card-header')
    ).nativeElement;
    const priceElement = fixture.nativeElement.querySelector(
      "p[product-name='priceElement']"
    );
    const ratingElement = fixture.debugElement.query(By.css('app-rating'));

    // then
    expect(cardElement.textContent).toContain('product de calitate');
    expect(priceElement.textContent).toContain('€1.00');
    expect(ratingElement).toBeDefined();
  });

  it('should call the sendProduct method on button click', () => {
    // given
    const sendProductSpy = spyOn(component, 'sendProduct');
    const buttonElement = fixture.nativeElement.querySelector(
      "button[button-name='sendProductButton']"
    );

    // when
    buttonElement.click();

    // then
    expect(sendProductSpy).toHaveBeenCalled();
  });

  it('should make a dispatch call on button click', () => {
    // given
    spyOn(component, 'sendProduct');
    const buttonElement = fixture.nativeElement.querySelector(
      "button[button-name='sendProductButton']"
    );

    // when
    buttonElement.click();

    // then
    expect(component.sendProduct).toHaveBeenCalled();
  });

  it('should go to url products/details/1', fakeAsync(() => {
    // given
    const buttonElement = fixture.nativeElement.querySelector(
      "button[button-name='routingButton']"
    );

    // when
    buttonElement.click();

    // then
    expect(router.navigate).toHaveBeenCalledOnceWith(['/products/details', 1]);
  }));
});
