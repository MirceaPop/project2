import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { NavbarComponent } from './navbar.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule, Store } from '@ngxs/store';
import { CartState } from '../ngxs/state/cart.state';
import { RouterTestingModule } from '@angular/router/testing';
import { ProductListWrapperComponent } from '../products/product-list-wrapper/product-list-wrapper.component';
import { AddProductComponent } from '../products/product-manager/add-product/add-product.component';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UpdateCartState } from '../ngxs/action/cart.actions';
import { HomeComponent } from '../home/home.component';

describe('NavbarComponent tests', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let store: Store;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavbarComponent,
        ProductListWrapperComponent,
        AddProductComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        NgxsModule.forRoot([CartState], {
          developmentMode: !environment.production,
        }),
        RouterTestingModule.withRoutes([
          {
            path: 'products',
            component: ProductListWrapperComponent,
          },
          {
            path: 'products/manager/new-product',
            component: AddProductComponent,
          },
          {
            path: 'home',
            component: HomeComponent,
          },
        ]),
      ],
    });

    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(Store);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should check that the component is defined', () => {
    expect(component).toBeTruthy();
  });

  it('it should test that the cart is updated', () => {
    // given
    const expectedBadgeValue = 3;
    store.dispatch(
      new UpdateCartState({
        products: [{ productId: 1, quantity: 3 }],
        size: 3,
        totalPrice: 3,
      })
    );

    // when
    fixture.detectChanges();
    const badgeElement = fixture.nativeElement.querySelector(
      "span[badge-name='badgeElement']"
    );

    // then
    expect(badgeElement.innerHTML).toBe(String(expectedBadgeValue));
  });

  [
    { linkName: 'productsLink', expectedResult: '/products' },
    {
      linkName: 'addProductLink',
      expectedResult: '/products/manager/new-product',
    },
    { linkName: 'homePageLink', expectedResult: '/home' },
  ].forEach((element) => {
    it('should test that products routing works and the links have active class', fakeAsync(() => {
      // given
      const linkElement = fixture.nativeElement.querySelector(
        `a[link-name=${element.linkName}]`
      );

      // when
      linkElement.click();
      tick();

      // then
      expect(router.url).toBe(element.expectedResult);
      expect(linkElement).toHaveClass('active');
    }));
  });
});
