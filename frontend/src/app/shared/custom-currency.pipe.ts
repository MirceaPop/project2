import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customCurrency',
})
export class CustomCurrencyPipe implements PipeTransform {
  transform(value: number | string, currencyCode?: string): string {
    return new Intl.NumberFormat('de-DE', {
      style: 'currency',
      currency: currencyCode || 'EUR',
      minimumFractionDigits: 2,
    })
      .format(Number(value))
      .replace(/\s+/g, ' ');
  }
}
