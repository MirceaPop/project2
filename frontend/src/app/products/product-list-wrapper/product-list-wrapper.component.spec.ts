import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { of } from 'rxjs';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { ErrorWrapperComponent } from 'src/app/shared/error-wrapper/error-wrapper.component';
import { RatingComponent } from 'src/app/shared/rating/rating.component';
import { NumberOfItems, ProductListModel, Sort } from '../product.model';
import { ProductCardComponent } from './product-card/product-card.component';
import { ProductFilterComponent } from './product-filter/product-filter.component';
import { ProductListWrapperComponent } from './product-list-wrapper.component';
import { ProductsPaginationComponent } from './products-pagination/products-pagination.component';
import { ProductsViewComponent } from './products-view/products-view.component';

const PRODUCTS_RESPONSE: ProductListModel = {
  total: 1,
  sizeResult: 1,
  products: [
    {
      id: 1,
      name: '',
      description: '',
      price: 1,
      rating: 1,
      image: '',
      tags: [],
    },
  ],
};

const DEFAULT_FILTERS = {
  tagData: '',
  priceOrder: Sort.ASC,
  ratingOrder: Sort.ASC,
  itemsNumber: NumberOfItems.FIVE,
  pageNumber: 0,
};

describe('ProductListWrapperComponent tests', () => {
  let component: ProductListWrapperComponent;
  let fixture: ComponentFixture<ProductListWrapperComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductListWrapperComponent,
        ProductFilterComponent,
        ProductsViewComponent,
        ProductsPaginationComponent,
        ProductCardComponent,
        RatingComponent,
        ErrorWrapperComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        NgxsModule.forRoot([ProductState]),
      ],
      providers: [],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should check that the component is rendered', () => {
    expect(component).toBeTruthy();
  });

  it('should check that the child-componenets are rendered', () => {
    // given
    const productFilterElement = fixture.debugElement.query(
      By.css('app-product-filter')
    );
    const productsViewElement = fixture.debugElement.query(
      By.css('app-products-view')
    );
    const productsPaginationElement = fixture.debugElement.query(
      By.css('app-products-pagination')
    );

    // then
    expect(productFilterElement).toBeTruthy();
    expect(productsViewElement).toBeTruthy();
    expect(productsPaginationElement).toBeTruthy();
  });

  it('should initialize filter-component with default filters', () => {
    // given
    const productFilterElement = fixture.debugElement.query(
      By.css('app-product-filter')
    );
    const productFilterInstance =
      productFilterElement.componentInstance as ProductFilterComponent;

    // then
    expect(productFilterInstance).toBeTruthy();
  });

  it('should check that the products-view component receives the input data', () => {
    // given
    const productsViewElement = fixture.debugElement.query(
      By.css('app-products-view')
    );
    const productsViewInstance =
      productsViewElement.componentInstance as ProductsViewComponent;
    Object.defineProperty(component, 'receivedProducts$', { writable: true });
    component.receivedProducts$ = of(PRODUCTS_RESPONSE);

    // when
    fixture.detectChanges();

    // then
    expect(productsViewInstance).toBeTruthy();
    expect(productsViewInstance.products).toEqual(PRODUCTS_RESPONSE.products);
  });

  it('should check that the products-pagination component receives the input data', () => {
    // given
    const productsPaginationElement = fixture.debugElement.query(
      By.css('app-products-pagination')
    );
    const productsPaginationInstance =
      productsPaginationElement.componentInstance as ProductsPaginationComponent;
    Object.defineProperty(component, 'receivedProducts$', { writable: true });
    component.receivedProducts$ = of(PRODUCTS_RESPONSE);

    // when
    fixture.detectChanges();

    // then
    expect(productsPaginationInstance).toBeTruthy();
    expect(productsPaginationInstance.totalItems).toEqual(1);
    expect(productsPaginationInstance.totalProductsPerPage).toBe(5);
  });

  it('should listen for filters changes', () => {
    // given
    spyOn(component, 'onFiltersChanged');
    const productFilterElement = fixture.debugElement.query(
      By.css('app-product-filter')
    );

    // when
    productFilterElement.triggerEventHandler(
      'sendFiltersData',
      DEFAULT_FILTERS
    );

    // then
    expect(component.onFiltersChanged).toHaveBeenCalledWith(DEFAULT_FILTERS);
  });

  it('should listen for page changes', () => {
    // given
    spyOn(component, 'onPageChanged');
    const productsPaginationElement = fixture.debugElement.query(
      By.css('app-products-pagination')
    );
    const pageNumber = 5;

    // when
    productsPaginationElement.triggerEventHandler('sendPageNumber', pageNumber);

    // then
    expect(component.onPageChanged).toHaveBeenCalledWith(pageNumber);
  });
});
