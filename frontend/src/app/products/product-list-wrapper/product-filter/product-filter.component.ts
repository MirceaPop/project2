import { Location } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  Subject,
  takeUntil,
} from 'rxjs';
import { NumberOfItems, ProductFiltersModel, Sort } from '../../product.model';

type DefaultValues = 0 | '' | Sort | NumberOfItems;
<<<<<<< HEAD

=======
>>>>>>> af070dba900195a426105115463b42161350b21f
@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.css'],
})
export class ProductFilterComponent implements OnInit {
  sort = Sort;
  pageSize = NumberOfItems;
  filtersForm: FormGroup;
  filtersFormValue: ProductFiltersModel;
  private destroy: Subject<boolean> = new Subject<boolean>();

  @Output() sendFiltersData = new EventEmitter<ProductFiltersModel>();

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.intializeFilters();
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  intializeFilters() {
    this.filtersFormValue = this.getQueryParamsAsFiltersModel();
    this.filtersForm = new FormGroup({
      tagData: new FormControl(this.filtersFormValue.tagData),
      priceOrder: new FormControl(this.filtersFormValue.priceOrder),
      ratingOrder: new FormControl(this.filtersFormValue.ratingOrder),
      itemsNumber: new FormControl(this.filtersFormValue.itemsNumber),
      pageNumber: new FormControl(this.filtersFormValue.pageNumber),
    });
<<<<<<< HEAD
    this.onFiltersChanged(this.filtersFormValue);

=======

    this.onFiltersChanged(this.filtersFormValue);

>>>>>>> af070dba900195a426105115463b42161350b21f
    this.filtersForm.controls['tagData'].valueChanges
      .pipe(
        takeUntil(this.destroy),
        debounceTime(500),
        map((value: string) => value.toLocaleLowerCase()),
        distinctUntilChanged(
          (previous: string, current: string) =>
            previous.toLocaleLowerCase() === current.toLocaleLowerCase()
        )
      )
      .subscribe((value) => {
        this.filtersFormValue = { ...this.filtersFormValue, tagData: value };
        this.onFiltersChanged(this.filtersFormValue);
      });

    Object.keys(this.filtersForm.controls)
      .filter((formControlName) => formControlName !== 'tagData')
      .forEach((formControlName) => {
        this.filtersForm.controls[formControlName].valueChanges
<<<<<<< HEAD
          .pipe(takeUntil(this.destroy))
=======
          .pipe(debounceTime(500))
>>>>>>> af070dba900195a426105115463b42161350b21f
          .subscribe((value) => {
            this.filtersFormValue = {
              ...this.filtersFormValue,
              [formControlName]: value,
            };
            this.onFiltersChanged(this.filtersFormValue);
          });
      });
  }

  private onFiltersChanged(filters: ProductFiltersModel) {
    this.modifiyQueryParams(filters);
    this.sendFiltersData.emit(filters);
  }

  private modifiyQueryParams(filters: ProductFiltersModel) {
    const url = this.router
      .createUrlTree([], {
        relativeTo: this.activatedRoute,
        queryParams: {
          pageNumber: filters.pageNumber,
          itemsNumber: filters.itemsNumber,
          priceOrder: filters.priceOrder,
          ratingOrder: filters.ratingOrder,
          tagData: filters.tagData,
        },
      })
      .toString();
    this.location.go(url);
  }
<<<<<<< HEAD
=======

>>>>>>> af070dba900195a426105115463b42161350b21f
  private getQueryParamsAsFiltersModel() {
    return {
      tagData: this.getQueryParamFilter('tagDate', ''),
      priceOrder: this.getQueryParamFilter('priceOrder', Sort.ASC),
      ratingOrder: this.getQueryParamFilter('ratingOrder', Sort.ASC),
      itemsNumber: this.getQueryParamFilter('itemsNumber', NumberOfItems.FIVE),
      pageNumber: this.getQueryParamFilter('pageNumber', 0),
    } as ProductFiltersModel;
  }

  private getQueryParamFilter(filterName: string, defaultValue: DefaultValues) {
    return (
      this.activatedRoute.snapshot.queryParamMap.get(filterName) || defaultValue
    );
  }
}
