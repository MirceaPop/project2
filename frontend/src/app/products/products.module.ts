import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductCardComponent } from './product-list-wrapper/product-card/product-card.component';
import { ProductFilterComponent } from './product-list-wrapper/product-filter/product-filter.component';
import { ProductListWrapperComponent } from './product-list-wrapper/product-list-wrapper.component';
import { ProductsPaginationComponent } from './product-list-wrapper/products-pagination/products-pagination.component';
import { ProductsViewComponent } from './product-list-wrapper/products-view/products-view.component';
import { AddProductComponent } from './product-manager/add-product/add-product.component';
import { AddTagsComponent } from './product-manager/add-tags/add-tags.component';
import { AvailableTagsComponent } from './product-manager/add-tags/available-tags/available-tags.component';
import { ProductManagerComponent } from './product-manager/product-manager.component';
import { ProductRoutingModule } from './product-routing.module';

@NgModule({
  declarations: [
    ProductDetailsComponent,
    ProductManagerComponent,
    ProductListWrapperComponent,
    ProductsViewComponent,
    ProductFilterComponent,
    ProductsPaginationComponent,
    ProductCardComponent,
    AddProductComponent,
    AddTagsComponent,
    AvailableTagsComponent
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    FormsModule,
    SharedModule,
    TranslateModule,
    ReactiveFormsModule
  ],
})
export class ProductsModule {}
