import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule, Store } from '@ngxs/store';
import { of } from 'rxjs';
import { UpdateCartState } from 'src/app/ngxs/action/cart.actions';
import { CartState } from 'src/app/ngxs/state/cart.state';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { ProductListWrapperComponent } from 'src/app/products/product-list-wrapper/product-list-wrapper.component';
import {
  ProductListModel,
  SimpleProductsModel,
} from 'src/app/products/product.model';
import { ProductService } from 'src/app/products/product.service';
import { CustomCurrencyPipe } from 'src/app/shared/custom-currency.pipe';
import { EmailService } from 'src/app/shared/email-service/email.service';
import { ErrorMessageComponent } from 'src/app/shared/error-message/error-message.component';
import { environment } from 'src/environments/environment';
import { PaymentFormComponent } from '../payment-form/payment-form.component';
import { ShoppingCartComponent } from './shopping-cart.component';

const PRODUCTS_RESPONSE: ProductListModel = {
  total: 1,
  sizeResult: 1,
  products: [
    {
      id: 1,
      name: '',
      description: '',
      price: 1,
      rating: 1,
      image: '',
      tags: [],
    },
  ],
};

const simpleProducts: SimpleProductsModel = {
  products: [
    {
      id: 1,
      name: '',
      description: '',
      price: 1,
      rating: 1,
      image: '',
      tags: [],
    },
  ],
};

describe('ShoppingCartComponent tests', () => {
  let component: ShoppingCartComponent;
  let fixture: ComponentFixture<ShoppingCartComponent>;
  let productServiceMock: jasmine.SpyObj<ProductService>;
  let emailServiceMock: jasmine.SpyObj<EmailService>;
  let store: Store;

  beforeEach(() => {
    productServiceMock = jasmine.createSpyObj<ProductService>(
      'ProductService',
      ['getProducts', 'getProductsAfterIds', 'getTags']
    );

    emailServiceMock = jasmine.createSpyObj<EmailService>('EmailService', [
      'checkEmailExistance',
    ]);

    productServiceMock.getProducts.and.returnValue(of(PRODUCTS_RESPONSE));
    productServiceMock.getProductsAfterIds.and.returnValue(of(simpleProducts));
    productServiceMock.getTags.and.returnValue(of());

    TestBed.configureTestingModule({
      declarations: [
        ShoppingCartComponent,
        CustomCurrencyPipe,
        PaymentFormComponent,
        ErrorMessageComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        NgxsModule.forRoot([CartState, ProductState]),
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([
          { path: 'products', component: ProductListWrapperComponent },
        ]),
      ],
      providers: [
        { provide: ProductService, useValue: productServiceMock },
        { provide: EmailService, useValue: emailServiceMock },
      ],
    });
    store = TestBed.inject(Store);
  });

  it('should check that the component is defined', () => {
    initializeComponent();
    expect(component).toBeTruthy();
  });

  [
    {
      input: {
        products: [
          {
            productId: 1,
            quantity: 3,
          },
        ],
        size: 3,
        totalPrice: 3,
      },
      expectedResults: {
        subtotalPrice: 2,
        quantity: 2,
      },
    },
    {
      input: {
        products: [
          {
            productId: 1,
            quantity: 1,
          },
        ],
        size: 1,
        totalPrice: 1,
      },
      expectedResults: {
        subtotalPrice: 1,
        quantity: 1,
      },
    },
  ].forEach((element) => {
    it('should check that the decrease button works correctly', fakeAsync(() => {
      // given
      store.dispatch(new UpdateCartState(element.input));
      initializeComponent();

      tick(2000);
      fixture.detectChanges();

      const inputElement = fixture.nativeElement.querySelector(
        "input[input-name='productValueElement']"
      );
      const decrementButtonElement = fixture.nativeElement.querySelector(
        "button[button-name='decreaseQuantityButton']"
      );

      // when
      decrementButtonElement.click();
      fixture.detectChanges();

      const subtotalPriceFormControl =
        component.productCartFormGroupsControls[0].value.totalPricePerItem;
      const quantityFormControl =
        component.productCartFormGroupsControls[0].value.quantity;
      const totalPrice = component.totalPrice;

      // then
      expect(inputElement.value).toBe(String(element.expectedResults.quantity));
      expect(quantityFormControl).toBe(element.expectedResults.quantity);
      expect(subtotalPriceFormControl).toBe(
        element.expectedResults.subtotalPrice
      );
      expect(totalPrice).toBe(element.expectedResults.subtotalPrice);
    }));
  });

  [
    {
      input: {
        products: [
          {
            productId: 1,
            quantity: 8,
          },
        ],
        size: 8,
        totalPrice: 8,
      },
      expectedResults: {
        subtotalPrice: 9,
        quantity: 9,
      },
    },
    {
      input: {
        products: [
          {
            productId: 1,
            quantity: 10,
          },
        ],
        size: 10,
        totalPrice: 10,
      },
      expectedResults: {
        subtotalPrice: 10,
        quantity: 10,
      },
    },
  ].forEach((element) => {
    it('should check that the increase button works correctly', fakeAsync(() => {
      // given
      store.dispatch(new UpdateCartState(element.input));
      initializeComponent();
      tick(2000);
      fixture.detectChanges();

      const inputElement = fixture.nativeElement.querySelector(
        "input[input-name='productValueElement']"
      );
      const incrementButtonElement = fixture.nativeElement.querySelector(
        "button[button-name='increaseQuantityButton']"
      );

      // when
      incrementButtonElement.click();
      fixture.detectChanges();

      const subtotalPriceFormControl =
        component.productCartFormGroupsControls[0].value.totalPricePerItem;
      const quantityFormControl =
        component.productCartFormGroupsControls[0].value.quantity;
      const totalPrice = component.totalPrice;

      // then
      expect(inputElement.value).toBe(String(element.expectedResults.quantity));
      expect(quantityFormControl).toBe(element.expectedResults.quantity);
      expect(subtotalPriceFormControl).toBe(
        element.expectedResults.subtotalPrice
      );
      expect(totalPrice).toBe(element.expectedResults.subtotalPrice);
    }));
  });

  [
    { inputValue: '0', expectedValue: '1' },
    { inputValue: '11', expectedValue: '10' },
    { inputValue: 'e', expectedValue: '1' },
    { inputValue: '-', expectedValue: '1' },
  ].forEach((element) => {
    it('should check that the input works correctly for given input values', fakeAsync(() => {
      // given
      initializeComponent();
      tick(2000);
      fixture.detectChanges();
      const inputElement = fixture.nativeElement.querySelector(
        "input[input-name='productValueElement']"
      );
      inputElement.value = element.inputValue;

      // when
      inputElement.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      const subtotalPriceFormControl =
        component.productCartFormGroupsControls[0].value.totalPricePerItem;
      const quantityFormControl =
        component.productCartFormGroupsControls[0].value.quantity;
      const totalPrice = component.totalPrice;

      // then
      expect(inputElement.value).toBe(element.expectedValue);
      expect(quantityFormControl).toBe(Number(element.expectedValue));
      expect(subtotalPriceFormControl).toBe(Number(element.expectedValue));
      expect(totalPrice).toBe(Number(element.expectedValue));
    }));
  });

  it('should check that the removeFromCart button works', fakeAsync(() => {
    // given
    initializeComponent();
    tick(2000);
    fixture.detectChanges();
    const removeFromCartButtonElement = fixture.nativeElement.querySelector(
      "button[button-name='removeItemButton']"
    );

    // when
    removeFromCartButtonElement.click();
    fixture.detectChanges();
    const formInputs = fixture.nativeElement.querySelector(
      "form[form-name='shoppingCartForm']"
    );

    // then
    expect(component.productCartFormGroups.length).toBe(0);
    expect(formInputs).toBeFalsy();
    expect(component.totalPrice).toBe(0);
  }));

  it('should check that the reset button works', fakeAsync(() => {
    // given
    initializeComponent();
    tick(2000);
    fixture.detectChanges();
    const resetButtonElement = fixture.nativeElement.querySelector(
      "button[button-name='resetButton']"
    );

    // when
    resetButtonElement.click();
    fixture.detectChanges();

    const formInputs = fixture.nativeElement.querySelector(
      "form[form-name='shoppingCartForm']"
    );

    // then
    expect(component.productCartFormGroups.length).toBe(0);
    expect(formInputs).toBeFalsy();
    expect(component.totalPrice).toBe(0);
  }));

  const initializeComponent = () => {
    fixture = TestBed.createComponent(ShoppingCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  };
});
