package com.example.internship.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Lob
    private String data;

    private String type;

    private String encodingMode;

    public Image() {
    }

    public Image(Long id, String data, String type, String encodingMode) {
        this.id = id;
        this.data = data;
        this.type = type;
        this.encodingMode = encodingMode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String uploadedImage) {
        this.data = uploadedImage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEncodingMode() {
        return encodingMode;
    }

    public void setEncodingMode(String encodingMode) {
        this.encodingMode = encodingMode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return Objects.equals(id, image.id) && Objects.equals(data, image.data) && Objects.equals(type, image.type) && Objects.equals(encodingMode, image.encodingMode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, data, type, encodingMode);
    }
}
