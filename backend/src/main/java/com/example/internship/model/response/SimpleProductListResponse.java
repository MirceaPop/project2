package com.example.internship.model.response;

import com.example.internship.model.Product;

import java.util.List;

public class SimpleProductListResponse {

    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
