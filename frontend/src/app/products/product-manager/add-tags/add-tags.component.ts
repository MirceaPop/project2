import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable, of, Subject, take, takeUntil } from 'rxjs';
import { GetTags, SendTag } from 'src/app/ngxs/action/product.actions';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { tagInputValidatorFactory } from 'src/app/shared/shared-validator/custom-validators';
import { TagModel } from '../../product.model';

@Component({
  selector: 'app-add-tags',
  templateUrl: './add-tags.component.html',
  styleUrls: ['./add-tags.component.css'],
})
export class AddTagsComponent implements OnInit {
  readonly NAME_INPUT_ERROR_MESSAGE =
    'Field is required, name must be at least 4 characters long and at max 10 and tag should not exist.';
  tagsList: TagModel[];
  tagsForm: FormGroup;
  private destroy: Subject<boolean> = new Subject<boolean>();

  constructor(private store: Store) {}

  @Select(ProductState.getTags)
  receivedTags$: Observable<TagModel[]>;

  ngOnInit() {
    this.store.dispatch(new GetTags());
    this.getTags();
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.tagsForm?.dirty) {
      const result = window.confirm('There are unsaved changes! Are you sure?');

      return of(result);
    }
    return true;
  }

  sendTag(tag: TagModel) {
    this.store.dispatch(new SendTag(tag));
    this.receivedTags$
      .pipe(takeUntil(this.destroy))
      .subscribe((tags: TagModel[]) => {
        this.tagsList = tags;
        this.tagsForm.reset();
      });
  }

  getTags() {
    this.receivedTags$
      .pipe(takeUntil(this.destroy))
      .subscribe((tags: TagModel[]) => {
        this.tagsList = tags;
        this.tagsForm = new FormGroup({
          name: new FormControl(null, [
            Validators.required,
            Validators.minLength(4),
            Validators.maxLength(10),
            tagInputValidatorFactory(tags),
          ]),
        });
      });
  }
}
