import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CartRoutingModule } from './cartRouting.module';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { PaymentFormComponent } from './payment-form/payment-form.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ShoppingCartComponent, PaymentFormComponent],
  imports: [
    CommonModule,
    CartRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    TranslateModule,
  ],
})
export class CartModule {}
