/**
 * Used just to store/retireve in localStorage
 */
export interface CartProductModel {
  productId: number;
  quantity: number;
}

export enum LocalStorageKeys {
  PRODUCTS = 'products',
}

export interface CartModel {
  products: CartProductModel[];
  size: number;
  totalPrice: number;
}
