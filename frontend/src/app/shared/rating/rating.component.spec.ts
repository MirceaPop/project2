import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RatingComponent } from './rating.component';

describe('RatingComponent tests', () => {
  let component: RatingComponent;
  let fixture: ComponentFixture<RatingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingComponent],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  [0, 1, 2, 3, 4].forEach((numberOfIcons) => {
    it(`should test that ${numberOfIcons} are checked `, () => {
      // given
      const totalItems: DebugElement[] = fixture.debugElement.queryAll(
        By.css('i')
      );
      component.productRating = numberOfIcons;

      // when
      fixture.detectChanges();

      // then
      expect(totalItems.length).toBe(5);
      expect(totalItems[numberOfIcons].classes['checked']).toBeTruthy();
    });
  });

  [0, 1, 2, 3, 4].forEach((number) => {
    it(`should emit ${number} on mouseover`, () => {
      // given
      spyOn(component.newItemEvent, 'emit');
      const totalItems: DebugElement[] = fixture.debugElement.queryAll(
        By.css('i')
      );

      // when
      totalItems[number].nativeElement.dispatchEvent(new Event('mouseover'));
      fixture.detectChanges();

      // then
      expect(component.newItemEvent.emit).toHaveBeenCalledWith(number);
    });
  });

  [0, 1, 2, 3].forEach((number) => {
    it(`should not emit ${number + 1} on mouseover`, () => {
      // given
      spyOn(component.newItemEvent, 'emit');
      const totalItems: DebugElement[] = fixture.debugElement.queryAll(
        By.css('i')
      );

      // when
      totalItems[number].nativeElement.dispatchEvent(new Event('click'));
      fixture.detectChanges();
      totalItems[number + 1].nativeElement.dispatchEvent(
        new Event('mouseover')
      );
      fixture.detectChanges();

      // then
      expect(component.newItemEvent.emit).not.toHaveBeenCalledWith(number + 1);
    });
  });
});
