package com.example.internship.repository;

import com.example.internship.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    @Query("SELECT product FROM Product product JOIN product.tags tag WHERE tag.name = :tag")
    Page<Product> findAllByTag(@Param("tag") String tag, Pageable pageable);

    @Query("SELECT COUNT(product) FROM Product product JOIN product.tags tag WHERE tag.name = :tag")
    Long countByTag(@Param("tag") String tag);

    Optional<Product> findById(Long id);
}
