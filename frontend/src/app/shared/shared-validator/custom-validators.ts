import { HttpResponse } from '@angular/common/http';
import {
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';
import { catchError, delay, map, Observable, of, switchMap } from 'rxjs';
import { TagModel } from '../../products/product.model';
import { EmailService } from '../email-service/email.service';

// This function return a synchronous validator for tag
export const tagInputValidatorFactory = (tagArray: TagModel[]): ValidatorFn => {
  const tagInputValidator: ValidatorFn = (
    control: AbstractControl
  ): ValidationErrors | null => {
    const hasValue = tagArray.find((element) => {
      return element.name === control.value;
    });
    return hasValue
      ? ({ controlName: { value: control.value } } as ValidationErrors)
      : null;
  };
  return tagInputValidator;
};

// This returns an asyncronus validator which checks if the email is on backend
export function emailValidatorFactory(
  emailService: EmailService
): AsyncValidatorFn {
  // This returns an Observable of type Validation error if there are any errors or null if not
  const asyncEmailValidator = (
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    // This transforms our formControl into an observable
    const $formControlObservable = of(control);
    // We make a call to our service and check if we have the email in our database.
    const $checkEmailExistanceObservable = emailService
      .checkEmailExistance(control.value)
      .pipe(
        // If the email exists we return null but if it doesn't exist we return an Observable of type ValidationErrors
        map((response: HttpResponse<void>) => {
          let emailExist: boolean = false;
          if (response.ok) {
            emailExist = true;
          } else {
            emailExist = false;
          }
          return emailExist ? null : { emailDoesNotExist: true };
        }),
        // If we have an error we return an Observable of type ValidationErrors
        catchError(() => of({ hasError: true }))
      );

    const $validatorResponseObservable: Observable<ValidationErrors | null> =
      $formControlObservable.pipe(
        // We apply a delay which we use to send our data to the next operator only if 500 ms have passed
        // and we didnt emit another value
        delay(500),
        // In switchMap we receive the data from delay and that means that we can make a call
        // to the backend and check for our email
        switchMap(() => $checkEmailExistanceObservable)
      );
    return $validatorResponseObservable;
  };
  return asyncEmailValidator;
}
