import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { CanComponentDeactivate, LeaveCheckGuard } from './leave-check.guard';

class DummyComponent implements CanComponentDeactivate {
  canDeactivate(): boolean | Observable<boolean> {
    return false;
  }
}

describe('CanDeactivateGuardService', () => {
  let mockComponent: jasmine.SpyObj<DummyComponent>;
  let guard: LeaveCheckGuard;

  beforeEach(() => {
    mockComponent = jasmine.createSpyObj('DummyComponent', ['canDeactivate']);
    TestBed.configureTestingModule({
      providers: [LeaveCheckGuard],
    });
    guard = TestBed.inject(LeaveCheckGuard);
  });

  it('should check that guard is instantiated', () => {
    expect(guard).toBeTruthy();
  });

  [true, false].forEach((expectedValue) => {
    it('should route for true and should not route for false', () => {
      // given
      mockComponent.canDeactivate.and.returnValue(expectedValue);

      // when
      const actualValue = guard.canDeactivate(mockComponent);

      // then
      expect(actualValue).toBe(expectedValue);
      expect(mockComponent.canDeactivate).toHaveBeenCalledOnceWith();
    });
  });

  [
    { returnedValue: of(true), expectedValue: true },
    { returnedValue: of(false), expectedValue: false },
  ].forEach((element) => {
    it('should route for observable<true> and shouldnt route for observable<false>  ', (done: DoneFn) => {
      // given
      mockComponent.canDeactivate.and.returnValue(element.returnedValue);

      // when
      const actualValue$ = guard.canDeactivate(
        mockComponent
      ) as Observable<boolean>;

      // then
      actualValue$.subscribe((actualValue) => {
        expect(actualValue).toBe(element.expectedValue);
        done();
      });
      expect(mockComponent.canDeactivate).toHaveBeenCalledOnceWith();
    });
  });
});
