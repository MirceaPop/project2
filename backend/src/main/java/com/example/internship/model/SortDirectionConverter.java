package com.example.internship.model;

import org.springframework.core.convert.converter.Converter;

public class SortDirectionConverter implements Converter<String, SortDirection> {
    @Override
    public SortDirection convert(String s) {
        return SortDirection.valueOf(s);
    }
}
