import { Component } from '@angular/core';

@Component({
  selector: 'app-new-product',
  templateUrl: './product-manager.component.html',
  styleUrls: ['./product-manager.component.css'],
})
export class ProductManagerComponent {}
