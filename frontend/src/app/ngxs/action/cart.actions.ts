import { ProductModel } from 'src/app/products/product.model';
import { CartModel } from 'src/app/shared/cart.model';

export class AddProductToCart {
  static readonly type = '[Cart State] AddProductToCart';
  constructor(public product: ProductModel) {}
}

export class UpdateCartState {
  static readonly type = '[Cart State] UpdateCartState';
  constructor(public cart: CartModel) {}
}

export class UpdatePaymentFormState {
  static readonly type = '[Cart State] UpdatePaymentFormState';
  constructor(public isDirty: boolean) {}
}
