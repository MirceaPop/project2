import { CartModel } from '../shared/cart.model';

export enum Sort {
  ASC = 'ASC',
  DESC = 'DESC',
}

export enum NumberOfItems {
  FIVE = 5,
  TEN = 10,
  TWENTY = 20,
  FIFTY = 50,
}

export enum ProductFormUseCase {
  EDIT,
  ADD,
}

export interface ProductListModel {
  total: number;
  sizeResult: number;
  products: ProductModel[];
}

export interface TagModel {
  id: number;
  name: string;
  isSelected?: boolean;
}

export interface ProductModel {
  id: number;
  name: string;
  description: string;
  price: number;
  rating: number;
  image: string;
  tags: TagModel[];
  quantity?: number;
}

export interface ProductFiltersModel {
  tagData: string;
  priceOrder: Sort;
  ratingOrder: Sort;
  itemsNumber: NumberOfItems;
  pageNumber: number;
}

export interface SimpleProductsModel {
  products: ProductModel[];
}

export interface CartAndProductsModel {
  cartModel: CartModel;
  productListResponse: SimpleProductsModel;
}

export interface FormGroupModel {
  id: number;
  image: string;
  name: string;
  price: number;
  quantity: number;
  totalPricePerItem: number;
}
