package com.example.internship.service;

import com.example.internship.model.Image;
import com.example.internship.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageService {

    private ImageRepository imageRepository;

    @Autowired
    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    public Image save(final Image image) {
        return imageRepository.save(image);
    }

    public void deleteImage(final Long id) {
        this.imageRepository.deleteById(id);
    }
}
