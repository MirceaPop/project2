import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { of } from 'rxjs';
import { CartState } from 'src/app/ngxs/state/cart.state';
import { EmailService } from 'src/app/shared/email-service/email.service';
import { ErrorMessageComponent } from 'src/app/shared/error-message/error-message.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { environment } from 'src/environments/environment';
import { PaymentFormComponent } from './payment-form.component';

describe('PaymentFormComponent tests', () => {
  let component: PaymentFormComponent;
  let fixture: ComponentFixture<PaymentFormComponent>;
  let emailServiceMock: jasmine.SpyObj<EmailService>;
  let firstNameInputElement: HTMLInputElement;
  let lastNameInputElement: HTMLInputElement;
  let emailInputElement: HTMLInputElement;
  let addressInputElement: HTMLInputElement;
  let submitButton: HTMLButtonElement;

  beforeEach(() => {
    emailServiceMock = jasmine.createSpyObj<EmailService>('EmailService', [
      'checkEmailExistance',
    ]);
    emailServiceMock.checkEmailExistance.and.returnValue(
      of(new HttpResponse({ status: 200 }) as HttpResponse<void>)
    );
    TestBed.configureTestingModule({
      declarations: [PaymentFormComponent, ErrorMessageComponent],
      imports: [
        TranslateModule.forRoot(),
        ReactiveFormsModule,
        SharedModule,
        NgxsModule.forRoot([CartState]),
        HttpClientTestingModule,
      ],
      providers: [{ provide: EmailService, useValue: emailServiceMock }],
    });

    fixture = TestBed.createComponent(PaymentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    submitButton = fixture.nativeElement.querySelector(
      "button[button-name='submit-button']"
    );
    intializeForm();
  });

  it('should check that the component is defined', () => {
    expect(component).toBeTruthy();
  });

  it('should check that all formControls are valid, the borders are green, and the form itself is valid', fakeAsync(() => {
    // when
    firstNameInputElement.dispatchEvent(new Event('input'));
    lastNameInputElement.dispatchEvent(new Event('input'));
    emailInputElement.dispatchEvent(new Event('input'));
    addressInputElement.dispatchEvent(new Event('input'));
    tick(500);
    fixture.detectChanges();

    // then
    expect(firstNameInputElement.style['borderColor']).toBe('green');
    expect(lastNameInputElement.style['borderColor']).toBe('green');
    expect(emailInputElement.style['borderColor']).toBe('green');
    expect(addressInputElement.style['borderColor']).toBe('green');
    expect(component.paymentForm.valid).toBeTruthy();
  }));

  [
    { invalidInput: '' },
    { invalidInput: 'mi' },
    { invalidInput: 'abcdefghksls' },
  ].forEach((element) => {
    it('should check that each formControl is invalid and the border is red if the input values dont respect the validators', () => {
      // given
      spyOn(component, 'submitPaymentForm');
      firstNameInputElement.value = element.invalidInput;
      lastNameInputElement.value = element.invalidInput;
      addressInputElement.value = element.invalidInput;

      // when
      firstNameInputElement.dispatchEvent(new Event('input'));
      lastNameInputElement.dispatchEvent(new Event('input'));
      addressInputElement.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      // then
      expect(component.paymentForm.get('firstName')?.valid).toBeFalsy();
      expect(component.paymentForm.get('lastName')?.valid).toBeFalsy();
      expect(component.paymentForm.get('address')?.valid).toBeFalsy();
      expect(firstNameInputElement.style['borderColor']).toBe('red');
      expect(lastNameInputElement.style['borderColor']).toBe('red');
      expect(addressInputElement.style['borderColor']).toBe('red');
    });
  });

  it('should check that the form is invalid and fields are empty after clicking on the submit button', fakeAsync(() => {
    // when
    firstNameInputElement.dispatchEvent(new Event('input'));
    lastNameInputElement.dispatchEvent(new Event('input'));
    emailInputElement.dispatchEvent(new Event('input'));
    addressInputElement.dispatchEvent(new Event('input'));
    tick(500);
    submitButton.dispatchEvent(new Event('click'));

    // then
    expect(firstNameInputElement.value).toBe('');
    expect(lastNameInputElement.value).toBe('');
    expect(emailInputElement.value).toBe('');
    expect(addressInputElement.value).toBe('');
    expect(component.paymentForm.valid).toBeFalsy();
  }));

  function intializeForm() {
    firstNameInputElement = fixture.debugElement.queryAll(By.css('input'))[0]
      .nativeElement;
    firstNameInputElement.value = 'mircea';

    lastNameInputElement = fixture.debugElement.queryAll(By.css('input'))[1]
      .nativeElement;
    lastNameInputElement.value = 'pop';

    emailInputElement = fixture.debugElement.queryAll(By.css('input'))[2]
      .nativeElement;
    emailInputElement.value = 'mircea@gmail.com';

    addressInputElement = fixture.debugElement.queryAll(By.css('input'))[3]
      .nativeElement;
    addressInputElement.value = 'baia mare';
  }
});
