package com.example.internship.service;

import com.example.internship.exception.ServiceException;
import com.example.internship.mapstruct.ProductMapper;
import com.example.internship.model.Image;
import com.example.internship.model.Product;
import com.example.internship.model.SortDirection;
import com.example.internship.model.dto.ProductDTO;
import com.example.internship.model.response.ImageWithFrontendIndex;
import com.example.internship.model.response.ProductListResponse;
import com.example.internship.model.response.SimpleProductListResponse;
import com.example.internship.repository.ImageRepository;
import com.example.internship.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.util.ObjectUtils.isEmpty;

@Service
public class ProductService {
    private final static Map<SortDirection, Sort.Direction> STRING_DIRECTION_MAP = initializeStringDirectionMap();

    private ProductRepository productRepository;
    private ImageRepository imageRepository;
    private ProductMapper productMapper;

    @Autowired
    public ProductService(ProductRepository productRepository, ImageRepository imageRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.imageRepository = imageRepository;
    }

    @Transactional
    public SimpleProductListResponse getProducts(final List<Long> products) {
        final SimpleProductListResponse response = new SimpleProductListResponse();
        response.setProducts((List<Product>) this.productRepository.findAllById(products));
        return response;
    }

    public ProductListResponse getAllProducts(final Integer page, final Integer size, final SortDirection priceSortDirection, final SortDirection ratingSortDirection, final String tag) {
        final Long totalSize = isEmpty(tag) ? productRepository.count() : productRepository.countByTag(tag);

        final Sort sortByPrice = getSortingRule("price", priceSortDirection);
        final Sort sortByRating = getSortingRule("rating", ratingSortDirection);
        final Pageable pageable = PageRequest.of(page, size, sortByRating.and(sortByPrice));

        final List<ProductDTO> filteredProducts = isEmpty(tag) ? productMapper.productListToProductDTOListMapper(productRepository.findAll(pageable).getContent()) : productMapper.productListToProductDTOListMapper(productRepository.findAllByTag(tag, pageable).getContent());

        return buildProductListResponse(totalSize, filteredProducts);
    }

    public ProductDTO getProductWithoutImage(final Long id) {
        final Optional<Product> product = productRepository.findById(id);
        if(product.isPresent()) {
            return productMapper.productToProductDTOMapper(product.get());
        }
        throw new ServiceException(String.format("Product with id %d doesn't exists!", id), HttpStatus.NOT_FOUND);
    }

    public Product getProductWithImage(final Long id) {
        return productRepository.findById(id).get();
    }

    @Transactional
    public Product createProduct(final Product product) {
        return productRepository.save(product);
    }

    public void deleteProduct(final Long id) {
        this.productRepository.deleteById(id);
    }

    public Image getProductImage(final Long productId) {
        return null;
    }

    public ImageWithFrontendIndex getProductImageWithFrontendIndex(final Long productId, final Long index) {
        return null;
    }

    private Sort getSortingRule(final String column, final SortDirection sortDirection) {
        return Sort.by(STRING_DIRECTION_MAP.get(sortDirection), column);
    }

    private static Map<SortDirection, Sort.Direction> initializeStringDirectionMap() {
        Map<SortDirection, Sort.Direction> map = new LinkedHashMap<>();
        map.put(SortDirection.ASC, Sort.Direction.ASC);
        map.put(SortDirection.DESC, Sort.Direction.DESC);
        return map;
    }

    private ProductListResponse buildProductListResponse(final Long totalSize, final List<ProductDTO> filteredProducts) {
        final ProductListResponse response = new ProductListResponse();
        response.setTotal(totalSize);
        response.setSizeResult(filteredProducts.size());
        response.setProducts(filteredProducts);
        return response;
    }
}
