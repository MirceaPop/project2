import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  postToLocalStorage<T>(key: string, value: T) {
    try {
      localStorage.setItem(key, JSON.stringify(value));
    } catch (e) {
      localStorage.setItem(key, `${value}`);
    }
  }

  getFromLocalStorage<T>(key: string): T | number | boolean | string | null {
    const item = localStorage.getItem(key);
    if (!!Number(item)) {
      return Number(item);
    } else if (item === 'true') {
      return true;
    } else if (item === 'false') {
      return false;
    }
    try {
      return JSON.parse(item || '') as T;
    } catch {
      return item;
    }
  }

  clearAll() {
    localStorage.clear();
  }
}
