import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import {
  UpdateCartState,
  UpdatePaymentFormState,
} from 'src/app/ngxs/action/cart.actions';
import { EmailService } from 'src/app/shared/email-service/email.service';
import { emailValidatorFactory } from 'src/app/shared/shared-validator/custom-validators';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.css'],
})
export class PaymentFormComponent implements OnInit {
  readonly NAME_INPUT_ERROR_MESSAGE =
    'Field is required, name must be at least 3 characters long and at max 10.';
  readonly EMAIL_INPUT_ERROR_MESSAGE = 'Email is not in our database.';

  paymentForm: FormGroup;
  @Output() newItemEvent = new EventEmitter<string>();

  constructor(private emailService: EmailService, private store: Store) {}

  get paymentFormFirstName() {
    return this.paymentForm.get('firstName');
  }

  get paymentFormLastName() {
    return this.paymentForm.get('lastName');
  }

  get paymentFormEmail() {
    return this.paymentForm.get('email');
  }

  get paymentFormAddress() {
    return this.paymentForm.get('address');
  }

  ngOnInit(): void {
    const emailValidator = emailValidatorFactory(this.emailService);
    this.paymentForm = new FormGroup({
      firstName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(10),
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(10),
      ]),
      email: new FormControl('', {
        validators: [Validators.required],
        asyncValidators: [emailValidator],
      }),
      address: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(10),
      ]),
    });
  }

  sendPaymentFormState() {
    this.store.dispatch(new UpdatePaymentFormState(this.paymentForm.dirty));
  }

  submitPaymentForm() {
    this.paymentForm.reset();
    this.store.dispatch(
      new UpdateCartState({
        products: [],
        size: 0,
        totalPrice: 0,
      })
    );
    this.newItemEvent.emit('clear');
    window.alert('Form submitted!');
    this.store.dispatch(new UpdatePaymentFormState(this.paymentForm.dirty));
  }
}
