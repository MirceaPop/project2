import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { of } from 'rxjs';
import { ProductState } from 'src/app/ngxs/state/product.state';
import { ErrorMessageComponent } from 'src/app/shared/error-message/error-message.component';
import { ProductService } from '../../product.service';
import { AddTagsComponent } from './add-tags.component';
import { AvailableTagsComponent } from './available-tags/available-tags.component';

describe('ProductsPaginationComponent tests', () => {
  let component: AddTagsComponent;
  let fixture: ComponentFixture<AddTagsComponent>;
  let productServiceMock: jasmine.SpyObj<ProductService>;

  productServiceMock = jasmine.createSpyObj<ProductService>('ProductService', [
    'getTags',
  ]);

  productServiceMock.getTags.and.returnValue(of([{ id: 1, name: 'name' }]));

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AddTagsComponent,
        ErrorMessageComponent,
        AvailableTagsComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        NgxsModule.forRoot([ProductState]),
        ReactiveFormsModule,
      ],
      providers: [{ provide: ProductService, useValue: productServiceMock }],
    });

    fixture = TestBed.createComponent(AddTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should check that the component is defined', () => {
    expect(component).toBeTruthy();
  });

  [
    { tagName: 'phone', expectedFormValidity: true },
    { tagName: 'ph', expectedFormValidity: false },
    { tagName: 'phwwwwwwwwwwww', expectedFormValidity: false },
  ].forEach((testsSuite) => {
    it('should check that the input and button work correctly', () => {
      // given
      const inputElement = fixture.nativeElement.querySelector(
        'input[input-name = "tagInputElement"]'
      );
      inputElement.value = testsSuite.tagName;

      // when
      inputElement.dispatchEvent(new Event('input'));

      // then
      expect(component.tagsForm.valid).toBe(testsSuite.expectedFormValidity);
    });
  });

  it('should check that the sendTag method is called', () => {
    // given
    spyOn(component, 'sendTag');
    const buttonElement = fixture.nativeElement.querySelector(
      'button[button-name = "submitButton"]'
    );
    const inputElement = fixture.nativeElement.querySelector(
      'input[input-name = "tagInputElement"]'
    );
    inputElement.value = 'tagg';

    // when
    inputElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    buttonElement.click();

    // then
    expect(component.sendTag).toHaveBeenCalled();
  });
});
