import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductManagerComponent } from './product-manager/product-manager.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductListWrapperComponent } from './product-list-wrapper/product-list-wrapper.component';
import { AddProductComponent } from './product-manager/add-product/add-product.component';
import { AddTagsComponent } from './product-manager/add-tags/add-tags.component';
import { ProductFormUseCase } from './product.model';
import { LeaveCheckGuard } from '../shared/shared-guards/leave-check.guard';

const routes: Routes = [
  {
    path: 'manager',
    component: ProductManagerComponent,
    children: [
      {
        path: 'new-product',
        component: AddProductComponent,
        data: { productFormUseCase: ProductFormUseCase.ADD },
        canDeactivate: [LeaveCheckGuard],
      },
      {
        path: 'new-tag',
        component: AddTagsComponent,
        canDeactivate: [LeaveCheckGuard],
      },
      {
        path: 'edit-product/:id',
        component: AddProductComponent,
        data: { productFormUseCase: ProductFormUseCase.EDIT },
        canDeactivate: [LeaveCheckGuard],
      },
    ],
  },
  { path: '', component: ProductListWrapperComponent },
  { path: 'details/:id', component: ProductDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductRoutingModule {}
