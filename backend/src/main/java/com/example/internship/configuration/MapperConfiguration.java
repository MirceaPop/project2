package com.example.internship.configuration;

import com.example.internship.mapstruct.ProductMapper;
import com.example.internship.mapstruct.ProductMapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfiguration {

    @Bean
    ProductMapper productMapper() {
        return new ProductMapperImpl();
    }
}
