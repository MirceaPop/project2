import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { hostUrl } from 'src/app/appConfig';
import { EmailService } from './email.service';

describe('EmailService tests', () => {
  let httpMock: HttpTestingController;
  let emailService: EmailService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EmailService],
    });
    emailService = TestBed.inject(EmailService);
    // httpTestingController allows for mocking and flushing requests
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should call with GET', (done: DoneFn) => {
    // given
    const email: string = 'mircea@gmail.com';
    const emailToCheck = emailService.checkEmailExistance(email);

    // when + then
    emailToCheck.subscribe(() => {
      done();
    });

    const request = httpMock.expectOne(
      `${hostUrl}/cart/email-check?email=mircea@gmail.com`
    );
    expect(request.request.method).toBe('GET');
    request.flush(email);
  });
});
