import { Component, Input } from '@angular/core';
import { TagModel } from 'src/app/products/product.model';

@Component({
  selector: 'app-available-tags',
  templateUrl: './available-tags.component.html',
  styleUrls: ['./available-tags.component.css'],
})
export class AvailableTagsComponent {
  @Input()
  tagsList: TagModel[];
}
