import { Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ErrorState } from 'src/app/ngxs/state/error.state';

@Component({
  selector: 'app-error-wrapper',
  templateUrl: './error-wrapper.component.html',
  styleUrls: ['./error-wrapper.component.css']
})
export class ErrorWrapperComponent {
  @Select(ErrorState.getErrors)
  receivedErrors$: Observable<string[]>;

  @Select(ErrorState.getSpinnerStatus)
  receivedSpinnerStatus$: Observable<boolean>;
}
