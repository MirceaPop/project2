import { MasterService } from './master.service';
import { ValueService } from './value.service';

describe('MasterService without Angular testing support', () => {
  let mockValueService: jasmine.SpyObj<ValueService>;
  let masterService: MasterService;

  beforeEach(() => {
    mockValueService = jasmine.createSpyObj('ValueService', [
      'isGreaterThanFive',
    ]);
    masterService = new MasterService(mockValueService);
  });

  [
    { param: -1, returnValue: false, expectedValue: '-1 is less than 5' },
    { param: 1, returnValue: false, expectedValue: '1 is less than 5' },
    { param: 4, returnValue: false, expectedValue: '4 is less than 5' },
    { param: 5, returnValue: true, expectedValue: '5 is greater than 5' },
    { param: 6, returnValue: true, expectedValue: '6 is greater than 5' },
  ].forEach((testInput) => {
    it(`should receive string ${testInput.expectedValue} when param = ${testInput.expectedValue}`, () => {
      // given
      mockValueService.isGreaterThanFive.and.returnValue(testInput.returnValue);

      // when
      const acutalValue = masterService.isGreaterThan(testInput.param);

      // then
      expect(acutalValue).toBe(testInput.expectedValue);
      expect(mockValueService.isGreaterThanFive).toHaveBeenCalledOnceWith(
        testInput.param
      );
    });
  });
});
