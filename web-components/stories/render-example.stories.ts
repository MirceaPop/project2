import { html } from 'lit';
import '../src/render-example/MyPage.js';

export default {
  title: 'MyPage',
  component: 'my-page',
};

const template = () => html`<my-page></my-page>`;

export const Test = template.bind({});
