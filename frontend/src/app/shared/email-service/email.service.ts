import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { hostUrl } from 'src/app/appConfig';

@Injectable({ providedIn: 'root' })
export class EmailService {
  constructor(private httpClient: HttpClient) {}

  checkEmailExistance(email: string): Observable<HttpResponse<void>> {
    const httpHeaders = new HttpHeaders({ responseType: 'application/json' });
    const httpParams = new HttpParams().set('email', email);
    return this.httpClient.get<void>(`${hostUrl}/cart/email-check`, {
      headers: httpHeaders,
      params: httpParams,
      observe: 'response',
    });
  }
}
