<<<<<<< HEAD
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
=======
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
>>>>>>> af070dba900195a426105115463b42161350b21f
import { GetProductsByFilters } from 'src/app/ngxs/action/product.actions';
import { ProductState } from 'src/app/ngxs/state/product.state';
import {
  ProductFiltersModel,
  ProductListModel,
} from 'src/app/products/product.model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list-wrapper',
  templateUrl: './product-list-wrapper.component.html',
  styleUrls: ['./product-list-wrapper.component.css'],
})
export class ProductListWrapperComponent {
  filters: ProductFiltersModel;

<<<<<<< HEAD
  constructor(private store: Store) {}
=======
  constructor(private store: Store, private productService: ProductService) {}
>>>>>>> af070dba900195a426105115463b42161350b21f

  @Select(ProductState.getProductsAfterFilters)
  receivedProducts$: Observable<ProductListModel>;

<<<<<<< HEAD
  onFiltersChanged(filters: ProductFiltersModel) {
    this.filters = filters;
=======
  ngOnInit() {}

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  onFiltersChanged(filters: ProductFiltersModel) {
>>>>>>> af070dba900195a426105115463b42161350b21f
    this.store.dispatch(new GetProductsByFilters(filters));
  }

  onPageChanged(event: number) {
    this.filters.pageNumber = event;
    this.onFiltersChanged(this.filters);
  }
}
