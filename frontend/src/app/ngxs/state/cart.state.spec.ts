import { TestBed } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { environment } from 'src/environments/environment';
import { AddProductToCart, UpdateCartState } from '../action/cart.actions';
import { CartState } from './cart.state';

describe('Cart State', () => {
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([CartState])],
    });

    store = TestBed.inject(Store);
    store.reset({
      cartState: {
        products: [],
        size: 0,
        totalPrice: 0,
      },
    });
  });

  it('should check that the addProductToCart action works and the state changes', () => {
    // given
    store.reset({
      cartState: {
        products: [],
        size: 1,
        totalPrice: 2,
      },
    });

    // when
    store.dispatch(
      new AddProductToCart({
        id: 1,
        name: '',
        description: '',
        price: 1,
        rating: 1,
        image: '',
        tags: [],
      })
    );

    const productsState = store.selectSnapshot(
      (state) => state.cartState.products
    );
    const sizeState = store.selectSnapshot((state) => state.cartState.size);
    const priceState = store.selectSnapshot(
      (state) => state.cartState.totalPrice
    );

    // then
    expect(productsState).toEqual([{ productId: 1, quantity: 1 }]);
    expect(sizeState).toEqual(2);
    expect(priceState).toEqual(3);
  });

  it('should check that the updateCartState action works and the state changes', () => {
    // when
    store.dispatch(
      new UpdateCartState({
        products: [{ productId: 2, quantity: 1 }],
        size: 3,
        totalPrice: 3,
      })
    );

    const productsState = store.selectSnapshot(
      (state) => state.cartState.products
    );
    const sizeState = store.selectSnapshot((state) => state.cartState.size);
    const priceState = store.selectSnapshot(
      (state) => state.cartState.totalPrice
    );

    // then
    expect(productsState).toEqual([{ productId: 2, quantity: 1 }]);
    expect(sizeState).toEqual(3);
    expect(priceState).toEqual(3);
  });
});
