import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import {
  concatMap,
  delay,
  map,
  Observable,
  of,
  Subject,
  Subscription,
  take,
  takeUntil,
  tap,
} from 'rxjs';
import { UpdateCartState } from 'src/app/ngxs/action/cart.actions';
import { GetProductsByIds } from 'src/app/ngxs/action/product.actions';
import { CartState } from 'src/app/ngxs/state/cart.state';
import { ProductState } from 'src/app/ngxs/state/product.state';
import {
  CartAndProductsModel,
  FormGroupModel,
  SimpleProductsModel,
} from 'src/app/products/product.model';
import { CartModel } from 'src/app/shared/cart.model';
import { DEFAULT_PRODUCT_IMAGE } from 'src/app/shared/constants';

enum CartInitializationEnum {
  LOADING = 'loading',
  EMPTY_CART = 'empty',
  NOT_EMPTY_CART = 'notEmpty',
}

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css'],
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
  totalPrice: number;
  productCartFormGroups: FormArray = new FormArray([]);
  isPaymentFormDirty: boolean = false;
  cartState: CartInitializationEnum = CartInitializationEnum.LOADING;
  loadingCartState = CartInitializationEnum.LOADING;
  emptyCartState = CartInitializationEnum.EMPTY_CART;
  notEmptyCartState = CartInitializationEnum.NOT_EMPTY_CART;

  private $unsubscribe = new Subject();
  private formGroupSubscriptions: Array<Subscription>;

  constructor(private router: Router, private store: Store) {}

  get productCartFormGroupsControls(): FormGroup[] {
    return this.productCartFormGroups.controls as FormGroup[];
  }

  @Select(CartState.getCartFromLocalStorage)
  receivedCart$: Observable<CartModel>;

  @Select(ProductState.getProductsAfterIds)
  getProductsAfterIds$: Observable<SimpleProductsModel>;

  @Select(CartState.getPaymentFormState)
  getFormState$: Observable<boolean>;

  ngOnInit() {
    this.getFormState$.subscribe((value) => (this.isPaymentFormDirty = value));
    this.receivedCart$
      .pipe(
        // take(1) - Il folosim pentru a prelua doar ultima valoare emisa, daca nu, atunci intra din nou pe onInit cand se emite o noua valoare pe $cart subject
        take(1),
        // tap() - Il folosim pentru a face modificari exterioare
        tap((cart) => (this.totalPrice = cart.totalPrice)),
        // concatMap() - Il folosim pentru a ne sincroniza functiile asincrone ($cart si getProductAfterIds)
        // Asteapta dupa fiecare valoare
        concatMap((cartModel) => {
          // Folosim map pentru a ne extrage id-urile returnand un array de id-uri
          const ids = cartModel.products.map((product) => product.productId);
          this.store.dispatch(new GetProductsByIds(ids));
          // Facem call la backend folosind array-ul de id-uri si returnam un Observable
          return this.getProductsAfterIds$.pipe(
            // Folosim map pentru a ne transforma valoarea si o returnam ca Observable
            map((productListResponse) => {
              return { cartModel, productListResponse } as CartAndProductsModel;
            })
          );
        }),
        // Doare pentru a folosi ngSwitch, fac un fake delay
        delay(2000)
      )
      // Folosim subscribe pentru a activa Observabelul si a asculta dupa valorile emise
      .subscribe(({ cartModel, productListResponse }: CartAndProductsModel) => {
        this.intializeForm(cartModel, productListResponse);
      });
  }
  // Il folosim pentru a face unsubscribe la subscriptiile noastre in momentul in care parasim pagina
  ngOnDestroy(): void {
    this.$unsubscribe.next(1);
    this.$unsubscribe.complete();
  }

  onNavigateToProductClick() {
    this.router.navigate(['/products']);
  }

  changeImage(index: number) {
    const formControl = this.productCartFormGroupsControls[index].get('image');
    formControl?.setValue(DEFAULT_PRODUCT_IMAGE, {
      emitEvent: false,
    });
  }

  updateQuantity(index: number, quantityUnit: number) {
    const formControl =
      this.productCartFormGroupsControls[index].get('quantity');
    if (quantityUnit === -1) {
      if (formControl?.value > 1) {
        formControl?.setValue(formControl.value + quantityUnit, {
          emitEvent: true,
        });
      }
    } else if (quantityUnit === 1) {
      if (formControl?.value < 9) {
        formControl?.setValue(formControl.value + quantityUnit, {
          emitEvent: true,
        });
      }
    }
  }

  clearAllItems() {
    this.productCartFormGroups.clear();
    this.updateCart();
  }

  removeFormGroup(index: number) {
    // In momentul in care stergem un produs trebuie sa facem unsubscribe pentru a evita leakurile de memorie
    this.formGroupSubscriptions
      .splice(index, 1)
      .forEach((subscription) => subscription.unsubscribe());
    this.productCartFormGroups.removeAt(index);
    this.updateCart();
  }

  checkIsValidKey(event: any) {
    if (event.which < 48 || event.which > 57) {
      event.preventDefault();
    }
  }

  checkIsValidInput(event: any, index: number) {
    if (event.target.value < 1) {
      this.productCartFormGroupsControls[index].get('quantity')?.setValue(1);
    }

    if (event.target.value > 10) {
      this.productCartFormGroupsControls[index].get('quantity')?.setValue(10);
    }
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.isPaymentFormDirty) {
      const result = window.confirm(
        'There are unsaved changes to your payment form! Are you sure?'
      );

      return of(result);
    }
    return true;
  }

  private intializeForm(
    cartModel: CartModel,
    productListResponse: SimpleProductsModel
  ) {
    this.productCartFormGroups = new FormArray([] as FormGroup[]);
    this.formGroupSubscriptions = new Array<Subscription>();
    productListResponse.products.forEach((product) => {
      const cartProduct = cartModel.products.find(
        (cartProduct) => cartProduct.productId === product.id
      );

      const cartProductFormGroup = new FormGroup({
        id: new FormControl(product.id),
        image: new FormControl(product.image),
        quantity: new FormControl(cartProduct?.quantity, [
          Validators.min(1),
          Validators.max(9),
        ]),
        price: new FormControl(product.price),
        name: new FormControl(product.name),
        totalPricePerItem: new FormControl(
          (cartProduct?.quantity || 0) * product.price
        ),
      });
      this.formGroupSubscriptions.push(
        this.subscribeFormGroupValueChanges(cartProductFormGroup)
      );
      this.productCartFormGroups.push(cartProductFormGroup);
    });
    this.cartState = this.productCartFormGroups.length
      ? CartInitializationEnum.NOT_EMPTY_CART
      : CartInitializationEnum.EMPTY_CART;
  }

  private subscribeFormGroupValueChanges(
    cartProductFormGroup: FormGroup
  ): Subscription {
    return (
      cartProductFormGroup.valueChanges
        // takeUntil() - Lasa valorile sa treaca pana cand un notifier (this.$unsubscribe) emite o valoare
        .pipe(takeUntil(this.$unsubscribe))
        .subscribe((formGroupValue: FormGroupModel) => {
          cartProductFormGroup
            .get('totalPricePerItem')
            ?.setValue(formGroupValue.price * formGroupValue.quantity, {
              emitEvent: false,
            });
          this.updateCart();
        })
    );
  }

  private updateCart() {
    const cartProducts = this.productCartFormGroups.value as FormGroupModel[];
    const productsAndQuantity = cartProducts.map((product: FormGroupModel) => {
      return { productId: product.id, quantity: product.quantity };
    });
    this.totalPrice = 0;
    let totalProducts = 0;
    cartProducts.forEach((currentProduct) => {
      let checkedProductQuantity = currentProduct.quantity;
      this.totalPrice += checkedProductQuantity * currentProduct.price;
      totalProducts += checkedProductQuantity;
    });
    const cart = {
      products: [...productsAndQuantity],
      size: totalProducts,
      totalPrice: this.totalPrice,
    };

    this.store.dispatch(new UpdateCartState(cart));

    this.cartState = this.productCartFormGroups.length
      ? CartInitializationEnum.NOT_EMPTY_CART
      : CartInitializationEnum.EMPTY_CART;
  }
}
