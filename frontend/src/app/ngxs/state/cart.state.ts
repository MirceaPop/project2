import { Injectable } from '@angular/core';
import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { ProductModel } from 'src/app/products/product.model';
import { CartModel, LocalStorageKeys } from 'src/app/shared/cart.model';
import { LocalStorageService } from 'src/app/shared/local-storage/local-storage.service';
import {
  AddProductToCart,
  UpdateCartState,
  UpdatePaymentFormState,
} from '../action/cart.actions';

interface CartStateModel extends CartModel {
  isPaymentFormDirty: boolean;
}

@State<CartStateModel>({
  name: 'cartState',
  defaults: {
    products: [],
    size: 0,
    totalPrice: 0,
    isPaymentFormDirty: false,
  },
})
@Injectable()
export class CartState implements NgxsOnInit {
  constructor(private localStorageService: LocalStorageService) {}

  ngxsOnInit(ctx: StateContext<CartStateModel>) {
    this.initializeState(ctx);
  }

  @Selector()
  static getTotalNumberOfProducts(state: CartStateModel): number {
    return state.size;
  }

  @Selector()
  static getPaymentFormState(state: CartStateModel): boolean {
    return state.isPaymentFormDirty;
  }

  @Selector()
  static getCartFromLocalStorage(state: CartStateModel): CartModel {
    return {
      products: state.products,
      size: state.size,
      totalPrice: state.totalPrice,
    };
  }

  @Action(AddProductToCart)
  addProductToCart(
    ctx: StateContext<CartStateModel>,
    action: AddProductToCart
  ): CartStateModel {
    const newState = this.updateStateWithNewProduct(action.product, {
      ...ctx.getState(),
    });

    this.localStorageService.postToLocalStorage<CartModel>(
      LocalStorageKeys.PRODUCTS,
      newState
    );
    return ctx.setState(newState);
  }

  @Action(UpdateCartState)
  updateCartState(
    ctx: StateContext<CartStateModel>,
    action: UpdateCartState
  ): CartStateModel {
    const newState = ctx.setState({
      ...ctx.getState(),
      products: action.cart.products,
      size: action.cart.size,
      totalPrice: action.cart.totalPrice,
    });

    this.localStorageService.postToLocalStorage<CartModel>(
      LocalStorageKeys.PRODUCTS,
      ctx.getState()
    );

    return newState;
  }

  @Action(UpdatePaymentFormState)
  updatePaymentFormState(
    ctx: StateContext<CartStateModel>,
    action: UpdatePaymentFormState
  ): CartStateModel {
    return ctx.setState({
      ...ctx.getState(),
      isPaymentFormDirty: action.isDirty,
    });
  }

  private initializeState(ctx: StateContext<CartStateModel>) {
    const valuesFromLocalStorage =
      this.localStorageService.getFromLocalStorage<CartModel>(
        LocalStorageKeys.PRODUCTS
      ) as CartModel | null;
    ctx.dispatch(
      new UpdateCartState(
        valuesFromLocalStorage || { products: [], size: 0, totalPrice: 0 }
      )
    );
  }

  // daca exista produs incrementam cu 1 cantitatea produsului respectiv, altfel adaugam un produs nou
  private updateStateWithNewProduct(
    product: ProductModel,
    cartModel: CartStateModel
  ): CartStateModel {
    cartModel.size += 1;
    cartModel.totalPrice += product.price;
    const idx = cartModel.products.findIndex(
      (element) => element.productId === product.id
    );
    if (idx === -1) {
      cartModel.products = [
        ...cartModel.products,
        { productId: product.id, quantity: 1 },
      ];
    } else {
      cartModel.products = [
        ...cartModel.products.slice(0, idx),
        {
          productId: product.id,
          quantity: cartModel.products[idx].quantity + 1,
        },
        ...cartModel.products.slice(idx + 1),
      ];
    }
    return cartModel;
  }
}
