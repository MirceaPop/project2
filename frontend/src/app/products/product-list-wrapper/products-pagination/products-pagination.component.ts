import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-products-pagination',
  templateUrl: './products-pagination.component.html',
  styleUrls: ['./products-pagination.component.css'],
})
export class ProductsPaginationComponent {
  totalProductsPerPage: number = 10;
  currentItemIndex: number = 0;
  pageList: any[];
  totalItems: number;
  numberOfPages: number;

  @Input()
  set productsPerPageFilter(numberOfProductsPerPage: number) {
    this.totalProductsPerPage = numberOfProductsPerPage;
    this.updateAndCheckInput();
  }

  @Input()
  set totalNumberOfItems(totalItems: number) {
    this.totalItems = totalItems;
    this.updateAndCheckInput();
  }

  @Output() sendPageNumber = new EventEmitter<number>();

  onNextChange() {
    this.currentItemIndex += 1;
    this.sendPageNumber.emit(this.currentItemIndex);
    this.modifyPagination(this.currentItemIndex);
  }

  onPrevChange() {
    this.currentItemIndex -= 1;
    this.sendPageNumber.emit(this.currentItemIndex);
    this.modifyPagination(this.currentItemIndex);
  }

  modifyPagination(i: number) {
    if (this.numberOfPages > 7) {
      if (i === 0) {
        this.pageList = [1, 2, 3, '...', this.numberOfPages];
      } else if (i === 1) {
        this.pageList = [1, 2, 3, 4, '...', this.numberOfPages];
      } else if (i === 2) {
        this.pageList = [1, 2, 3, 4, 5, '...', this.numberOfPages];
      } else if (i === 3) {
        this.pageList = [1, 2, 3, 4, 5, 6, '...', this.numberOfPages];
      } else if (i === this.numberOfPages - 1) {
        this.pageList = [
          1,
          '...',
          this.numberOfPages - 2,
          this.numberOfPages - 1,
          this.numberOfPages,
        ];
      } else if (i === this.numberOfPages - 2) {
        this.pageList = [
          1,
          '...',
          this.numberOfPages - 3,
          this.numberOfPages - 2,
          this.numberOfPages - 1,
          this.numberOfPages,
        ];
      } else if (i === this.numberOfPages - 3) {
        this.pageList = [
          1,
          '...',
          this.numberOfPages - 4,
          this.numberOfPages - 3,
          this.numberOfPages - 2,
          this.numberOfPages - 1,
          this.numberOfPages,
        ];
      } else if (i === this.numberOfPages - 4) {
        this.pageList = [
          1,
          '...',
          this.numberOfPages - 5,
          this.numberOfPages - 4,
          this.numberOfPages - 3,
          this.numberOfPages - 2,
          this.numberOfPages - 1,
          this.numberOfPages,
        ];
      } else if (i > 3 && i < this.numberOfPages - 4) {
        this.pageList = [
          1,
          '...',
          i - 1,
          i,
          i + 1,
          i + 2,
          i + 3,
          '...',
          this.numberOfPages,
        ];
      }
    }
  }

  updateAndCheckInput() {
    if (this.totalItems && this.totalProductsPerPage) {
      this.numberOfPages = Math.ceil(
        this.totalItems / this.totalProductsPerPage
      );
      this.pageList = [...Array(this.numberOfPages).keys()];
      if (this.numberOfPages < 8) {
        this.pageList.shift();
        this.pageList.push(this.numberOfPages);
      } else if (this.numberOfPages > 7) {
        this.pageList = [1, 2, 3, '...', this.numberOfPages];
      }
      this.currentItemIndex = 0;
    }
  }

  sendNumber(item: any) {
    if (Number.isInteger(item)) {
      this.currentItemIndex = item;
      this.sendPageNumber.emit(item);
      this.modifyPagination(item);
    }
  }
}
