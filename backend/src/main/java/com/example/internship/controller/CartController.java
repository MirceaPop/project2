package com.example.internship.controller;
import com.example.internship.model.response.SimpleProductListResponse;
import com.example.internship.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.lang.*;

@RequestMapping("/cart")
@RestController
public class CartController {
    private static final Set<String> EMAILS = new HashSet<>(Arrays.asList("mircea@gmail.com"));

    final private ProductService productService;

    public CartController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("email-check")
    public ResponseEntity checkEmail(@RequestParam("email") String email){
        try {
            // thread to sleep for 1000 milliseconds
            Thread.sleep(5000);
         } catch (Exception e) {
            System.out.println(e);
         }
        return EMAILS.contains(email) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }

    @GetMapping("products")
    public ResponseEntity<SimpleProductListResponse> getProducts(@RequestParam("productId") List<Long> products) {
        return ResponseEntity.ok(this.productService.getProducts(products));
    }
}
