import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatingComponent } from './rating/rating.component';
import { InputValidatorDirective } from './shared-input-validator/input-validator.directive';
import { ErrorMessageComponent } from './error-message/error-message.component';
import { ErrorWrapperComponent } from './error-wrapper/error-wrapper.component';
import { CustomCurrencyPipe } from './custom-currency.pipe';

@NgModule({
  declarations: [
    RatingComponent,
    InputValidatorDirective,
    ErrorMessageComponent,
    ErrorWrapperComponent,
    CustomCurrencyPipe,
  ],
  imports: [CommonModule],
  exports: [
    RatingComponent,
    InputValidatorDirective,
    ErrorMessageComponent,
    ErrorWrapperComponent,
    CustomCurrencyPipe,
  ],
})
export class SharedModule {}
