import { ValueService } from './value.service';

describe('ValueServiceService', () => {
  let valueService: ValueService;

  beforeEach(() => {
    valueService = new ValueService();
  });

  it('should return true for value greater than 5', () => {
    const result = valueService.isGreaterThanFive(6);
    expect(result).toBe(true);
  });

  it('should return false for value smaller than 5', () => {
    const result = valueService.isGreaterThanFive(4);
    expect(result).toBe(false);
  });

  it('should return added value of 4', () => {
    const result = valueService.addValues(2, 2);
    expect(result).toBe(4);
  });

  it('should return added value of 8', () => {
    const result = valueService.addValues(4, 4);
    expect(result).toBe(8);
  });
});
