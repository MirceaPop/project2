package com.example.internship.controller;

import com.example.internship.model.Image;
import com.example.internship.model.Product;
import com.example.internship.model.SortDirection;
import com.example.internship.model.dto.ProductDTO;
import com.example.internship.model.response.ImageWithFrontendIndex;
import com.example.internship.model.response.ProductListResponse;
import com.example.internship.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public ProductListResponse getAllProducts(@RequestParam(name = "pageNo") Integer page,
                                              @RequestParam(name = "pageSize") Integer size,
                                              @RequestParam("priceSort") SortDirection priceSortDirection,
                                              @RequestParam("ratingSort") SortDirection ratingSortDirection,
                                              @RequestParam(value = "tag", required = false) String tag) {
        return productService.getAllProducts(page, size, priceSortDirection, ratingSortDirection, tag);
    }

    @GetMapping("/{id}")
    public ProductDTO getProductWithoutImage(@PathVariable Long id) {
        return productService.getProductWithoutImage(id);
    }

    @GetMapping("/product-with-image/{id}")
    public Product getProductWithImage(@PathVariable Long id) {
        return productService.getProductWithImage(id);
    }

    @GetMapping("image/{id}")
    public Image getProductImage(@PathVariable Long id) {
        return productService.getProductImage(id);
    }

    /**
     * Retrieves the image for the product that is indicated by the given id and returns an object
     * that contains both the requested image and the given index
     *
     * @param id    the product id
     * @param index the index that has to be sent back as it is
     * @return an object that contains both the requested image and the given index
     * @throws Exception
     */
    @GetMapping("image-with-frontend-index")
    public ImageWithFrontendIndex getProductImageWithFrontendIndex(@RequestParam(name = "id") Long id, @RequestParam(name = "index") Long index) {
        return productService.getProductImageWithFrontendIndex(id, index);
    }

    @PostMapping()
    public Product createProduct(@RequestBody Product product) {
        return productService.createProduct(product);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
        return ResponseEntity.noContent().build();
    }
}
